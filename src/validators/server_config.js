import {
  validateChain,
  validateInteger,
  validateMaybeTrimmedString,
  validateMissing,
  validateNonEmptyTrimmedString,
  validateOption,
  validateString,
  validateStringToJson,
  validateStringToNumber,
  validateTest,
  validateUrl,
} from "./core"

function validateDb(data) {
  if (data === null || data === undefined) {
    return [data, "Missing value"]
  }
  if (typeof data !== "object") {
    return [data, `Expected an object got "${typeof data}"`]
  }

  data = { ...data }
  const errors = {}
  const remainingKeys = new Set(Object.keys(data))

  for (let key of ["database", "host", "password", "user"]) {
    remainingKeys.delete(key)
    const [value, error] = validateNonEmptyTrimmedString(data[key])
    data[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  {
    const key = "port"
    remainingKeys.delete(key)
    const [value, error] = validateChain([
      validateStringToNumber,
      validateInteger,
      validateTest(
        value => 0 <= value && value <= 65536,
        "Must be an integer between 0 and 65536",
      ),
    ])(data[key])
    data[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  for (let key of remainingKeys) {
    errors[key] = "Unexpected item"
  }
  return [data, Object.keys(errors).length === 0 ? null : errors]
}

function validateGitLab(data) {
  if (data === null || data === undefined) {
    return [data, "Missing value"]
  }
  if (typeof data !== "object") {
    return [data, `Expected an object got "${typeof data}"`]
  }

  data = { ...data }
  const errors = {}
  const remainingKeys = new Set(Object.keys(data))

  for (let key of ["accessToken", "projectPath"]) {
    remainingKeys.delete(key)
    const [value, error] = validateNonEmptyTrimmedString(data[key])
    data[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  {
    const key = "url"
    remainingKeys.delete(key)
    const [value, error] = validateUrl(data[key])
    data[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  for (let key of remainingKeys) {
    errors[key] = "Unexpected item"
  }
  return [data, Object.keys(errors).length === 0 ? null : errors]
}

function validateNextcloud(data) {
  if (data === null || data === undefined) {
    return [data, "Missing value"]
  }
  if (typeof data !== "object") {
    return [data, `Expected an object got "${typeof data}"`]
  }

  data = { ...data }
  const errors = {}
  const remainingKeys = new Set(Object.keys(data))

  for (let key of ["password", "user", "userAgent"]) {
    remainingKeys.delete(key)
    const [value, error] = validateNonEmptyTrimmedString(data[key])
    data[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  {
    const key = "url"
    remainingKeys.delete(key)
    const [value, error] = validateUrl(data[key])
    data[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  for (let key of remainingKeys) {
    errors[key] = "Unexpected item"
  }
  return [data, Object.keys(errors).length === 0 ? null : errors]
}

function validateQuiz(data) {
  if (data === null || data === undefined) {
    return [data, "Missing value"]
  }
  if (typeof data !== "object") {
    return [data, `Expected an object got "${typeof data}"`]
  }

  data = { ...data }
  const errors = {}
  const remainingKeys = new Set(Object.keys(data))

  for (let key of ["segment", "token"]) {
    remainingKeys.delete(key)
    const [value, error] = validateNonEmptyTrimmedString(data[key])
    data[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  {
    const key = "url"
    remainingKeys.delete(key)
    const [value, error] = validateUrl(data[key])
    data[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  for (let key of remainingKeys) {
    errors[key] = "Unexpected item"
  }
  return [data, Object.keys(errors).length === 0 ? null : errors]
}

function validateSaml(data) {
  if (data === null || data === undefined) {
    return [data, "Missing value"]
  }
  if (typeof data !== "object") {
    return [data, `Expected an object got "${typeof data}"`]
  }

  data = { ...data }
  const errors = {}
  const remainingKeys = new Set(Object.keys(data))

  for (let key of [
    "cert",
    "decryptionCertificatePath",
    "decryptionPrivateKeyPath",
    "identifierFormat",
    "signatureCertificatePath",
    "signaturePrivateKeyPath",
  ]) {
    remainingKeys.delete(key)
    const [value, error] = validateMaybeTrimmedString(data[key])
    data[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  for (let key of ["entryPoint", "logoutUrl", "url"]) {
    remainingKeys.delete(key)
    const [value, error] = validateUrl(data[key])
    data[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  for (let key of ["issuer"]) {
    remainingKeys.delete(key)
    const [value, error] = validateNonEmptyTrimmedString(data[key])
    data[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  for (let key of remainingKeys) {
    errors[key] = "Unexpected item"
  }
  return [data, Object.keys(errors).length === 0 ? null : errors]
}

export function validateServerConfig(data) {
  if (data === null || data === undefined) {
    return [data, "Missing value"]
  }
  if (typeof data !== "object") {
    return [data, `Expected an object got "${typeof data}"`]
  }

  data = { ...data }
  const errors = {}
  const remainingKeys = new Set(Object.keys(data))

  for (let key of ["dataDir", "sessionSecret"]) {
    remainingKeys.delete(key)
    const [value, error] = validateNonEmptyTrimmedString(data[key])
    data[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  {
    const key = "db"
    remainingKeys.delete(key)
    const [value, error] = validateDb(data[key])
    data[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  {
    const key = "devUser"
    remainingKeys.delete(key)
    const [value, error] = validateOption([
      validateMissing,
      [validateString, validateStringToJson],
    ])(data[key])
    data[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  {
    const key = "gitlab"
    remainingKeys.delete(key)
    const [value, error] = validateOption([validateMissing, validateGitLab])(
      data[key],
    )
    data[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  {
    const key = "nextcloud"
    remainingKeys.delete(key)
    const [value, error] = validateOption([validateMissing, validateNextcloud])(
      data[key],
    )
    data[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  {
    const key = "quiz"
    remainingKeys.delete(key)
    const [value, error] = validateOption([validateMissing, validateQuiz])(
      data[key],
    )
    data[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  {
    const key = "saml"
    remainingKeys.delete(key)
    const [value, error] = validateOption([validateMissing, validateSaml])(
      data[key],
    )
    data[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  {
    const key = "telegram"
    remainingKeys.delete(key)
    const [value, error] = validateOption([validateMissing, validateTelegram])(
      data[key],
    )
    data[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  {
    const key = "zendesk"
    remainingKeys.delete(key)
    const [value, error] = validateOption([validateMissing, validateZendesk])(
      data[key],
    )
    data[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  for (let key of remainingKeys) {
    errors[key] = "Unexpected item"
  }
  return [data, Object.keys(errors).length === 0 ? null : errors]
}

function validateTelegram(data) {
  if (data === null || data === undefined) {
    return [data, "Missing value"]
  }
  if (typeof data !== "object") {
    return [data, `Expected an object got "${typeof data}"`]
  }

  data = { ...data }
  const errors = {}
  const remainingKeys = new Set(Object.keys(data))

  for (let key of ["apiHash", "apiId", "binaryPath", "phoneNumber"]) {
    remainingKeys.delete(key)
    const [value, error] = validateNonEmptyTrimmedString(data[key])
    data[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  for (let key of ["authorizationCode"]) {
    remainingKeys.delete(key)
    const [value, error] = validateMaybeTrimmedString(data[key])
    data[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  for (let key of remainingKeys) {
    errors[key] = "Unexpected item"
  }
  return [data, Object.keys(errors).length === 0 ? null : errors]
}

function validateZendesk(data) {
  if (data === null || data === undefined) {
    return [data, "Missing value"]
  }
  if (typeof data !== "object") {
    return [data, `Expected an object got "${typeof data}"`]
  }

  data = { ...data }
  const errors = {}
  const remainingKeys = new Set(Object.keys(data))

  for (let key of ["actualitesId", "apiToken", "email"]) {
    remainingKeys.delete(key)
    const [value, error] = validateNonEmptyTrimmedString(data[key])
    data[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  {
    const key = "url"
    remainingKeys.delete(key)
    const [value, error] = validateUrl(data[key])
    data[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  for (let key of remainingKeys) {
    errors[key] = "Unexpected item"
  }
  return [data, Object.keys(errors).length === 0 ? null : errors]
}
