import {
  validateArray,
  validateFunction,
  validateMaybeTrimmedString,
  validateMissing,
  validateNonEmptyTrimmedString,
  validateOption,
  validateString,
} from "./core"

export function validateSamlProfile(samlProfile) {
  if (samlProfile === null || samlProfile === undefined) {
    return [samlProfile, "Missing value"]
  }
  if (typeof samlProfile !== "object") {
    return [samlProfile, `Expected an object got "${typeof samlProfile}"`]
  }

  samlProfile = { ...samlProfile }
  const errors = {}
  const remainingKeys = new Set(Object.keys(samlProfile))

  for (let key of [
    "division",
    "email",
    "fullname",
    "idTribun",
    "manager",
    "mail",
  ]) {
    remainingKeys.delete(key)
    const [value, error] = validateMaybeTrimmedString(samlProfile[key])
    samlProfile[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  {
    const key = "memberof"
    remainingKeys.delete(key)
    const [value, error] = validateOption([
      validateMissing,
      [
        validateString,
        validateNonEmptyTrimmedString,
        validateFunction(value => [value]),
      ],
      validateArray(validateNonEmptyTrimmedString),
    ])(samlProfile[key])
    samlProfile[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  for (let key of ["nameID", "saml_id"]) {
    remainingKeys.delete(key)
    const [value, error] = validateNonEmptyTrimmedString(samlProfile[key])
    samlProfile[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  // Ignore unexpected SAML fields => The lines below are commented out.
  // for (let key of remainingKeys) {
  //   errors[key] = "Unexpected item"
  // }
  return [samlProfile, Object.keys(errors).length === 0 ? null : errors]
}
