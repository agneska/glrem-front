import { Store } from "svelte/store.js"

import * as sapper from "../__sapper__/client.js"

// eslint-disable-next-line no-undef
fetch("auth/me", { credentials: "same-origin" })
  .then(response => response.json())
  .then(user => {
    sapper.start({
      store: data => {
        return new Store({
          ...data,
          user,
        })
      },
      // eslint-disable-next-line no-undef
      target: document.querySelector("#sapper"),
    })
  })
  .catch(error => console.error(error))
