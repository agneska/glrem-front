import assert from "assert"
import commandLineArgs from "command-line-args"
import * as dav from "dav"
// dav.debug.enabled = true
import dedent from "dedent-js"
import ical from "ical.js"
import { loadAssembleeData } from "@tricoteuses/assemblee"
import { EnabledDatasets } from "@tricoteuses/assemblee/lib/config"
import { Etat } from "@tricoteuses/assemblee/lib/types/agendas"
import url from "url"

import { rainbow } from "../lib/colors"
import { slugify } from "../lib/strings"
import serverConfig from "../server_config"

const { nextcloud } = serverConfig

const optionsDefinitions = [
  {
    alias: "l",
    defaultValue: "15",
    name: "legislature",
    type: String,
  },
  {
    alias: "s",
    help: "don't log anything",
    name: "silent",
    type: Boolean,
  },
  {
    defaultOption: true,
    help: "directory containing Assemblée open data files",
    name: "dataDir",
    type: String,
  },
]
const options = commandLineArgs(optionsDefinitions)
const { acteurByUid, organeByUid, reunionByUid } = loadAssembleeData(
  options.dataDir,
  EnabledDatasets.Agendas | EnabledDatasets.ActeursEtOrganes,
  options.legislature,
)

function escapeXml(s) {
  return s
    .replace(/&apos;/g, "'")
    .replace(/&quot;/g, '"')
    .replace(/&gt;/g, ">")
    .replace(/&lt;/g, "<")
    .replace(/&amp;/g, "&")
}

async function main() {
  const xhr = new dav.transport.Basic(
    new dav.Credentials({
      username: nextcloud.user,
      password: nextcloud.password,
    }),
  )
  const account = await dav.createAccount({
    accountType: "caldav",
    // loadObjects: true,
    server: url.resolve(nextcloud.url, "remote.php/dav/"),
    xhr,
  })
  const calendarByDisplayName = account.calendars.reduce(
    (calendarByDisplayName, calendar) => {
      calendarByDisplayName[calendar.displayName] = calendar
      return calendarByDisplayName
    },
    {},
  )
  const objectByUrlByCalendarDisplayName = {}
  for (let reunion of Object.values(reunionByUid)) {
    if (![Etat.Confirmé, Etat.Eventuel].includes(reunion.cycleDeVie.etat)) {
      continue
    }
    const reunionCalendarsDisplayNames = []
    const organeReuni = undefined
    if (reunion.organeReuniRef !== undefined) {
      organeReuni = organeByUid[reunion.organeReuniRef]
      if (organeReuni === undefined && !options.silent) {
        console.log(`Organe réuni inconnu: ${reunion.organeReuniRef}`)
      }
    }
    if (organeReuni === undefined) {
      const demandeurs = reunion.demandeurs
      if (demandeurs === undefined) {
        reunionCalendarsDisplayNames.push("Réunions orphelines")
      } else {
        if (demandeurs.organe !== undefined) {
          const organe = organeByUid[demandeurs.organe.organeRef]
          if (organe === undefined) {
            if (!options.silent) {
              console.log(`Organe demandeur inconnu: ${demandeurs.organe}`)
            }
          } else {
            assert(organe.libelleAbrege)
            reunionCalendarsDisplayNames.push(organe.libelleAbrege)
          }
        }
        for (let acteurSummary of demandeurs.acteurs || []) {
          const acteur = acteurByUid[acteurSummary.acteurRef]
          if (acteur === undefined) {
            if (!options.silent) {
              console.log(`Acteur demandeur inconnu: ${acteurSummary}`)
            }
            continue
          }
          reunionCalendarsDisplayNames.push(
            `${acteur.etatCivil.ident.nom}, ${acteur.etatCivil.ident.prenom}`,
          )
        }
        if (reunionCalendarsDisplayNames.length === 0) {
          reunionCalendarsDisplayNames.push("Réunions orphelines")
        }
      }
    } else {
      assert(organeReuni.libelleAbrege)
      reunionCalendarsDisplayNames.push(organeReuni.libelleAbrege)
    }
    for (let calendarDisplayName of reunionCalendarsDisplayNames) {
      let calendar = calendarByDisplayName[calendarDisplayName]
      let calendarObjectByUrl = undefined
      if (calendar === undefined) {
        // Calendar doesn't exist in Nextcloud yet. Create it.
        const calendarSlug = slugify(calendarDisplayName)
        assert(calendarSlug)
        const calendarUrl = url.resolve(
          nextcloud.url,
          `remote.php/dav/calendars/${nextcloud.user}/${calendarSlug}/`,
        )
        if (!options.silent) {
          console.log(
            `Creating calendar "${calendarDisplayName}" at ${calendarUrl}…`,
          )
        }
        const color = rainbow(1000, Object.keys(calendarByDisplayName).length)
        const request = dav.request.basic({
          method: "MKCOL",
          data: dedent`
            <d:mkcol xmlns:d="DAV:">
              <d:set>
                <d:prop>
                  <d:resourcetype>
                    <d:collection/>
                    <c:calendar xmlns:c="urn:ietf:params:xml:ns:caldav"/>
                  </d:resourcetype>
                  <d:displayname>${escapeXml(
                    calendarDisplayName,
                  )}</d:displayname>
                  <a:calendar-color xmlns:a="http://apple.com/ns/ical/">${color}</a:calendar-color>
                  <o:calendar-enabled xmlns:o="http://owncloud.org/ns">1</o:calendar-enabled>
                  <c:supported-calendar-component-set xmlns:c="urn:ietf:params:xml:ns:caldav">
                    <c:comp name="VEVENT"/>
                    <c:comp name="VTODO"/>
                  </c:supported-calendar-component-set>
                </d:prop>
              </d:set>
            </d:mkcol>
          `,
        })
        const response = await xhr.send(request, calendarUrl)
        assert.strictEqual(response.request.status, 201)

        // It is sufficient to add a dummy calendar to calendarByDisplayName.
        calendar = calendarByDisplayName[calendarDisplayName] = {
          account,
          displayName: calendarDisplayName,
          url: calendarUrl,
        }
        calendarObjectByUrl = objectByUrlByCalendarDisplayName[
          calendarDisplayName
        ] = {}
      } else {
        calendarObjectByUrl =
          objectByUrlByCalendarDisplayName[calendarDisplayName]
        if (calendarObjectByUrl === undefined) {
          // Retrieve existing objects in Nextcloud calendar.
          if (!options.silent) {
            console.log(
              `Loading existing objects from calendar "${calendarDisplayName}" at ${
                calendar.url
              }`,
            )
          }
          const calendarObjects = await dav.listCalendarObjects(calendar, {
            xhr,
          })
          calendarObjectByUrl = objectByUrlByCalendarDisplayName[
            calendarDisplayName
          ] = calendarObjects.reduce((calendarObjectByUrl, calendarObject) => {
            calendarObjectByUrl[calendarObject.url] = calendarObject
            return calendarObjectByUrl
          }, {})
        }
      }

      const vcalendar = new ical.Component(["vcalendar", [], []])
      vcalendar.updatePropertyWithValue(
        "prodid",
        "-//Tricoteuses.fr Tricoteuses-Assemblée",
      )

      const vevent = new ical.Component("vevent")
      const event = new ical.Event(vevent)

      // Cf http://mozilla-comm.github.io/ical.js/api/ICAL.Event.html
      const odj = reunion.odj
      if (odj) {
        const description = odj.convocationOdj
          ? odj.convocationOdj.join("\n")
          : odj.pointsOdj
          ? odj.pointsOdj
              .filter(pointOdj =>
                [Etat.Confirmé, Etat.Eventuel].includes(
                  pointOdj.cycleDeVie.etat,
                ),
              )
              .map(pointOdj => pointOdj.objet)
              .join("\n")
          : odj.resumeOdj
          ? odj.resumeOdj.join("\n")
          : null
        if (description !== null) {
          event.description = description
        }
      }
      if (reunion.timestampFin) {
        event.endDate = ical.Time.fromJSDate(reunion.timestampFin)
      }
      const lieu = reunion.lieu
      if (lieu) {
        const location =
          lieu.libelleLong || lieu.libelleCourt || lieu.code || null
        if (location !== null) {
          event.location = location
        }
      }
      event.startDate = ical.Time.fromJSDate(reunion.timestampDebut)
      let summary
      if (event.description === null) {
        summary = "Réservation de salle"
      } else {
        summary = event.description.replace(/\n/g, " ")
        if (summary.length > 80) {
          summary = summary.substring(0, 99) + "…"
        }
      }
      event.summary = summary
      event.uid = reunion.uid

      // Set custom properties.
      if (reunion.formatReunion !== undefined && reunion.formatReunion !== "Ordinaire") {
        vevent.addPropertyWithValue("x-format-reunion", reunion.formatReunion)
      }
      if (reunion.ouverturePresse === "true") {
        vevent.addPropertyWithValue("x-ouverture-presse", "true")
      }

      // Add the new component
      vcalendar.addSubcomponent(vevent)
      const vcalendarString = vcalendar.toString()

      const filename = `${reunion.uid}.ics`
      const calendarObjectUrl = url.resolve(calendar.url, filename)
      const calendarObject = calendarObjectByUrl[calendarObjectUrl]
      if (calendarObject === undefined) {
        if (!options.silent) {
          console.log(
            `In calendar "${calendarDisplayName}", creating ${reunion.timestampDebut.toISOString()}: ${summary}`,
          )
        }
        await dav.createCalendarObject(calendar, {
          data: vcalendarString,
          filename,
          xhr,
        })
      } else {
        const vcalendarStringUnified = vcalendarString
          .replace(/\r\n/g, "\n")
          .replace(/\n /g, "")
          .trim()
        const calendarDataUnified = calendarObject.calendarData
          .replace("\nVERSION:2.0\n", "\n")
          .replace(/\nDTSTAMP:\d{8}T\d{6}Z\n/, "\n")
          .replace(/\r\n/g, "\n")
          .replace(/\n /g, "")
          .trim()
        if (vcalendarStringUnified !== calendarDataUnified) {
          if (!options.silent) {
            console.log(
              `In calendar "${calendarDisplayName}", updating ${reunion.timestampDebut.toISOString()}: ${summary}`,
            )
            console.log("Before:")
            console.log(calendarDataUnified)
            console.log(calendarDataUnified.length)
            console.log("After:")
            console.log(vcalendarStringUnified)
            console.log(vcalendarStringUnified.length)
            console.log()
          }
          calendarObject.calendarData = vcalendarString
          await dav.updateCalendarObject(calendarObject, {
            xhr,
          })
        }

        delete calendarObjectByUrl[calendarObjectUrl]
      }
    }
  }

  for (let [calendarDisplayName, calendarObjectByUrl] of Object.entries(
    objectByUrlByCalendarDisplayName,
  )) {
    for (let calendarObject of Object.values(calendarObjectByUrl)) {
      if (!options.silent) {
        console.log(
          `In calendar "${calendarDisplayName}", deleting ${
            calendarObject.calendarData
          }`,
        )
      }
      await dav.deleteCalendarObject(calendarObject, {
        xhr,
      })
    }
  }
}

main().catch(error => {
  console.log(error)
  process.exit(1)
})
