import assert from "assert"
import commandLineArgs from "command-line-args"
import fs from "fs-extra"
import path from "path"
import { loadAssembleeData } from "@tricoteuses/assemblee"
import { EnabledDatasets } from "@tricoteuses/assemblee/lib/config"
import {
  CodeType,
  MandatXsiType,
} from "@tricoteuses/assemblee/lib/types/acteurs_et_organes"

const optionsDefinitions = [
  {
    alias: "g",
    defaultValue: "PO730964",
    help: 'uid of "groupe parlementaire LaREM"',
    name: "groupeParlementaire",
    type: String,
  },
  {
    alias: "l",
    defaultValue: "15",
    name: "legislature",
    type: String,
  },
  {
    defaultOption: true,
    help: "directory containing Assemblée open data files",
    name: "dataDir",
    type: String,
  },
]
const options = commandLineArgs(optionsDefinitions)

async function writeDeputesEtOrganes() {
  const dataDir = options.dataDir
  assert(dataDir, "Missing argument: data directory")
  const { acteurByUid, infosDeputeByUid, organeByUid, photoDeputeByUid } = loadAssembleeData(
    options.dataDir,
    EnabledDatasets.ActeursEtOrganes | EnabledDatasets.InfosDeputes | EnabledDatasets.PhotosDeputes,
    options.legislature,
  )

  const now = new Date()
  const deputeActifByUid = {}
  const infosDeputeActifByUid = {}
  const organesActifsUids = new Set()
  const photoDeputeActifByUid = {}
  for (const acteur of Object.values(acteurByUid)) {
    const { mandats } = acteur
    if (!mandats) {
      continue
    }
    const acteurOrganesUids = new Set()
    let isMemberGroupeParlementaire = false
    let commissionPermanenteUid = null
    let mandatParlementaire = null
    for (const mandat of mandats) {
      if (
        mandat.xsiType === MandatXsiType.MandatParlementaireType &&
        mandat.legislature === options.legislature &&
        mandat.typeOrgane === CodeType.Assemblee &&
        mandat.dateDebut <= now &&
        (!mandat.dateFin || mandat.dateFin >= now)
      ) {
        mandatParlementaire = mandat
      }
      if (
        mandat.xsiType === MandatXsiType.MandatSimpleType &&
        mandat.legislature === options.legislature &&
        mandat.typeOrgane === CodeType.Gp &&
        mandat.dateDebut <= now &&
        (!mandat.dateFin || mandat.dateFin >= now) &&
        mandat.organesRefs.some(
          organeRef => organeRef == options.groupeParlementaire,
        )
      ) {
        isMemberGroupeParlementaire = true
      }
      if (
        mandat.xsiType === MandatXsiType.MandatSimpleType &&
        mandat.legislature === options.legislature &&
        mandat.typeOrgane === CodeType.Comper &&
        mandat.dateDebut <= now &&
        (!mandat.dateFin || mandat.dateFin >= now)
      ) {
        commissionPermanenteUid = mandat.organesRefs[0]
      }
      for (const organeRef of mandat.organesRefs) {
        acteurOrganesUids.add(organeRef)
      }
    }
    if (mandatParlementaire == null || !isMemberGroupeParlementaire) {
      continue
    }
    acteur.commissionPermanenteUid = commissionPermanenteUid
    acteur.departementCirco = mandatParlementaire.election.lieu.departement
    deputeActifByUid[acteur.uid] = acteur

    const infosDeputeActif = infosDeputeByUid[acteur.uid]
    if (infosDeputeActif !== undefined) {
      infosDeputeActifByUid[acteur.uid] = infosDeputeActif
    }

    for (const organeUid of acteurOrganesUids) {
      organesActifsUids.add(organeUid)
    }

    const photoDeputeActif = photoDeputeByUid[acteur.uid]
    if (photoDeputeActif !== undefined) {
      photoDeputeActifByUid[acteur.uid] = photoDeputeActif
    }
  }

  const organeActifByUid = {}
  for (const organeUid of organesActifsUids) {
    const organe = organeByUid[organeUid]
    if (organe !== undefined) {
      organeActifByUid[organeUid] = organe
    }
  }

  const jsonFilePath = path.join(dataDir, "deputes_et_organes.json")
  fs.writeFileSync(
    jsonFilePath,
    JSON.stringify(
      {
        deputeByUid: deputeActifByUid,
        infosDeputeByUid: infosDeputeActifByUid,
        organeByUid: organeActifByUid,
        photoDeputeByUid: photoDeputeActifByUid,
      },
      null,
      2,
    ),
    {
      encoding: "utf8",
    },
  )
}

writeDeputesEtOrganes().catch(error => {
  console.log(error)
  process.exit(1)
})

// vim: set ts=2 sw=2 sts=2 et:
