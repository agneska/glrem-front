import assert from "assert"
import commandLineArgs from "command-line-args"
import { loadAssembleeData } from "@tricoteuses/assemblee"
import { EnabledDatasets } from "@tricoteuses/assemblee/lib/config"
import {
  CodeType,
  MandatXsiType,
} from "@tricoteuses/assemblee/lib/types/acteurs_et_organes"

import { db } from "../database"

const optionsDefinitions = [
  {
    alias: "g",
    defaultValue: "PO730964",
    help: 'uid of "groupe parlementaire LaREM"',
    name: "groupeParlementaire",
    type: String,
  },
  {
    alias: "l",
    defaultValue: "15",
    name: "legislature",
    type: String,
  },
  {
    defaultOption: true,
    help: "directory containing Assemblée open data files",
    name: "dataDir",
    type: String,
  },
]
const options = commandLineArgs(optionsDefinitions)
const { acteurByUid, organeByUid } = loadAssembleeData(
  options.dataDir,
  EnabledDatasets.ActeursEtOrganes | EnabledDatasets.InfosDeputes,
  options.legislature,
)

function getDeputesActifs() {
  const dataDir = options.dataDir
  assert(dataDir, "Missing argument: data directory")
  const now = new Date()
  const deputesActifs = Object.values(acteurByUid)
    .filter(acteur => {
      const { mandats } = acteur
      if (!mandats) {
        return false
      }
      const mandatDeputeActif = mandats.filter(
        mandat =>
          mandat.xsiType === MandatXsiType.MandatParlementaireType &&
          mandat.legislature === options.legislature &&
          mandat.typeOrgane === CodeType.Assemblee &&
          mandat.dateDebut <= now &&
          (!mandat.dateFin || mandat.dateFin >= now),
      )[0]
      if (mandatDeputeActif === undefined) {
        return false
      }
      const mandatGroupeParlementaireActif = mandats.filter(
        mandat =>
          mandat.xsiType === MandatXsiType.MandatSimpleType &&
          mandat.legislature === options.legislature &&
          mandat.typeOrgane === CodeType.Gp &&
          mandat.dateDebut <= now &&
          (!mandat.dateFin || mandat.dateFin >= now) &&
          mandat.organesRefs.some(
            organeRef => organeRef == options.groupeParlementaire,
          ),
      )[0]
      if (mandatGroupeParlementaireActif === undefined) {
        return false
      }
      return true
    })
    .sort((a, b) => a.uid.localeCompare(b.uid))

  const firstUid = options.uid
  let skip = !!firstUid
  for (let depute of deputesActifs) {
    if (skip) {
      if (depute.uid === firstUid) {
        skip = false
      } else {
        continue
      }
    }
  }

  return deputesActifs
}

function getDisplayNamesAnnuairesDepute(depute) {
  const displayNames = new Set()
  const now = new Date()

  const ident = depute.etatCivil.ident
  displayNames.add(`${ident.nom}, ${ident.prenom}`)

  for (const mandat of depute.mandats) {
    if (mandat.dateDebut <= now && (!mandat.dateFin || mandat.dateFin >= now)) {
      for (const organeRef of mandat.organesRefs) {
        const organe = organeByUid[organeRef]
        if (organe !== undefined) {
          displayNames.add(organe.libelleAbrege)
        }
      }
    }
  }

  return [...displayNames].sort()
}

async function main() {
  // Retrieve the IDs of the existing active "députés".
  const existingDeputesActifsUids = new Set(
    (await db.any(
      `
      SELECT aut_id
      FROM auteurs
      WHERE aut_actif
    `,
    )).map(({ aut_id }) => aut_id),
  )

  const deputesActifs = getDeputesActifs()
  for (let depute of deputesActifs) {
    await db.none(
      `
        INSERT INTO auteurs (
          aut_id,
          aut_auth_id,
          aut_civ,
          aut_nom,
          aut_prenom,
          aut_alpha,
          aut_trigramme,
          aut_annuaires,
          aut_actif
        )
        VALUES (
          $<uid>,
          NULL,
          $<civ>,
          $<nom>,
          $<prenom>,
          $<alpha>,
          $<trigramme>,
          $<annuaires>,
          true
        )
        ON CONFLICT (aut_id)
        DO UPDATE SET
          aut_civ = $<civ>,
          aut_nom = $<nom>,
          aut_prenom = $<prenom>,
          aut_alpha = $<alpha>,
          aut_trigramme = $<trigramme>,
          aut_annuaires = $<annuaires>,
          aut_actif = true
      `,
      {
        ...depute.etatCivil.ident,
        annuaires: getDisplayNamesAnnuairesDepute(depute),
        uid: depute.uid,
      },
    )
    existingDeputesActifsUids.delete(depute.uid)
  }

  // Set the obsolete active "députés" to inactive.
  for (const inactiveDeputeUid of existingDeputesActifsUids) {
    await db.none(
      `
        UPDATE auteurs
        SET aut_actif = false
        WHERE aut_id = $<inactiveDeputeUid>
      `,
      {
        inactiveDeputeUid,
      }
    )
  }
}

main().catch(error => {
  console.log(error)
  process.exit(1)
})

// vim: set ts=2 sw=2 sts=2 et:
