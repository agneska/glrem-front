/**
 * Return a JSON error to HTTP.
 */
export function jsonError(res, code, message) {
  res.writeHead(code, {
    "Content-Type": "application/json; charset=utf-8",
  })
  return res.end(
    JSON.stringify(
      {
        error: {
          code,
          message,
        },
      },
      null,
      2,
    ),
  )
}

// vim: set ts=2 sw=2 sts=2 et:
