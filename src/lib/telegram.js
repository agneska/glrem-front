import { Client as TelegramClient } from "tglib"
import path from "path"

import serverConfig from "../server_config"
import {
  indexWebSocketServer,
  sendDataToWebSocketClients,
} from "../lib/web_socket_servers"

const filesDir = path.join(process.cwd(), "__tglib__", "files")
const { telegram } = serverConfig
const thumbnailsDir = path.join(
  process.cwd(),
  "__tglib__",
  "database",
  "thumbnails",
)

export const telegramClient = telegram
  ? new TelegramClient({
      apiId: telegram.apiId,
      apiHash: telegram.apiHash,
      binaryPath: telegram.binaryPath,
    })
  : null

export async function setupFile(file) {
  const { local } = file
  if (local.is_downloading_completed) {
    // Replace path of file with its URL.
    local.url = local.path
      .replace(filesDir, "chats/files")
      .replace(thumbnailsDir, "chats/thumbnails")
    delete local.path
  } else if (!local.is_downloading_active) {
    await telegramClient.fetch({
      "@type": "downloadFile",
      file_id: file.id,
      priority: 1,
      offset: 0,
      limit: 0,
      synchronous: false,
    })
  }
}

export async function setupMessageContent(content) {
  switch (content["@type"]) {
    case "messageDocument":
      await setupFile(content.document.document)
      if (content.document.thumbnail) {
        await setupFile(content.document.thumbnail.photo)
      }
      break
    case "messagePhoto":
      for (let photoSize of content.photo.sizes) {
        await setupFile(photoSize.photo)
      }
      break
    case "messageText":
      if (content.web_page && content.web_page.photo) {
        for (let photoSize of content.web_page.photo.sizes) {
          await setupFile(photoSize.photo)
        }
      }
      break
    default:
      break
  }
}

export async function setupTelegram() {
  if (telegramClient === null) {
    return
  }
  telegramClient.registerCallback("td:update", async update => {
    switch (update["@type"]) {
      case "ok":
        break
      case "updateBasicGroupFullInfo":
        break
      case "updateBasicGroup":
        // {
        //   "@type": "updateBasicGroup",
        //   "basic_group": {
        //     "@type": "basicGroup",
        //     "id": 332481416,
        //     "member_count": 6,
        //     "status": {
        //       "@type": "chatMemberStatusAdministrator",
        //       "can_be_edited": false,
        //       "can_change_info": true,
        //       "can_post_messages": false,
        //       "can_edit_messages": false,
        //       "can_delete_messages": true,
        //       "can_invite_users": true,
        //       "can_restrict_members": true,
        //       "can_pin_messages": false,
        //       "can_promote_members": false
        //     },
        //     "everyone_is_administrator": true,
        //     "is_active": true,
        //     "upgraded_to_supergroup_id": 0
        //   }
        // }
        break
      case "updateChatDraftMessage":
        break
      case "updateChatLastMessage":
        // {
        //   "@type": "updateChatLastMessage",
        //   "chat_id": -123456789,
        //   "last_message": {
        //     "@type": "message",
        //     "id": 1635773213,
        //     "sender_user_id": 123456780,
        //     "chat_id": -123456789,
        //     "is_outgoing": false,
        //     "can_be_edited": false,
        //     "can_be_forwarded": true,
        //     "can_be_deleted_only_for_self": true,
        //     "can_be_deleted_for_all_users": false,
        //     "is_channel_post": false,
        //     "contains_unread_mention": false,
        //     "date": 1556978085,
        //     "edit_date": 0,
        //     "reply_to_message_id": 0,
        //     "ttl": 0,
        //     "ttl_expires_in": 0,
        //     "via_bot_user_id": 0,
        //     "author_signature": "",
        //     "views": 0,
        //     "media_album_id": "0",
        //     "content": {
        //       "@type": "messageText",
        //       "text": {
        //         "@type": "formattedText",
        //         "text": "Bravo 👍",
        //         "entities": []
        //       }
        //     }
        //   },
        //   "order": "0"
        // }
        await setupMessageContent(update.last_message.content)
        sendDataToWebSocketClients(indexWebSocketServer, update)
        break
      case "updateChatNotificationSettings":
        // {
        //   "@type": "updateChatNotificationSettings",
        //   "chat_id": 872676993,
        //   "notification_settings": {
        //     "@type": "chatNotificationSettings",
        //     "use_default_mute_for": true,
        //     "mute_for": 0,
        //     "use_default_sound": true,
        //     "sound": "default",
        //     "use_default_show_preview": true,
        //     "show_preview": false,
        //     "use_default_disable_pinned_message_notifications": true,
        //     "disable_pinned_message_notifications": false,
        //     "use_default_disable_mention_notifications": true,
        //     "disable_mention_notifications": false
        //   }
        // }
        break
      case "updateChatOrder":
        break
      case "updateChatPinnedMessage":
        // {
        //   "@type": "updateChatPinnedMessage",
        //   "chat_id": -1001346979214,
        //   "pinned_message_id": 351272960
        // }
        break
      case "updateChatReadInbox":
        // {
        //   "@type": "updateChatReadInbox",
        //   "chat_id": -1001103318029,
        //   "last_read_inbox_message_id": 300941312,
        //   "unread_count": 9
        // }
        break
      case "updateChatReadOutbox":
        break
      case "updateChatUnreadMentionCount":
      // {
      //   "@type": "updateChatUnreadMentionCount",
      //   "chat_id": -332481416,
      //   "unread_mention_count": 1
      // }
      case "updateConnectionState":
        // {
        //   "@type": "updateConnectionState",
        //   "state": {
        //     "@type": "connectionStateConnecting"
        //   }
        // }
        break
      case "updateDeleteMessages":
        // {
        //   "@type": "updateDeleteMessages",
        //   "chat_id": 757602995,
        //   "message_ids": [
        //     57626591232,
        //     58050215936,
        //     58051264512,
        //     58052313088,
        //     58053361664
        //   ],
        //   "is_permanent": false,
        //   "from_cache": true
        // }
        if (update.is_permanent) {
          sendDataToWebSocketClients(indexWebSocketServer, update)
        }
        break
      case "updateFile":
        // {
        //   "@type": "updateFile",
        //   "file": {
        //     "@type": "file",
        //     "id": 158,
        //     "size": 693,
        //     "expected_size": 693,
        //     "local": {
        //       "@type": "localFile",
        //       "path": "",
        //       "can_be_downloaded": true,
        //       "can_be_deleted": false,
        //       "is_downloading_active": true,
        //       "is_downloading_completed": false,
        //       "downloaded_prefix_size": 0,
        //       "downloaded_size": 0
        //     },
        //     "remote": {
        //       "@type": "remoteFile",
        //       "id": "AgADBAADkrAxGx_eEVJMJZV4C4bFbeD7vBkABCJzvG_3XmgxiJsBAAEC",
        //       "is_uploading_active": false,
        //       "is_uploading_completed": true,
        //       "uploaded_size": 693
        //     }
        //   }
        // }
        await setupFile(update.file)
        sendDataToWebSocketClients(indexWebSocketServer, update)
        break
      case "updateHavePendingNotifications":
        // {
        //   "@type": "updateHavePendingNotifications",
        //   "have_delayed_notifications": false,
        //   "have_unreceived_notifications": true
        // }
        break
      case "updateMessageContent":
        // {
        //   "@type": "updateMessageContent",
        //   "chat_id": -1001346979214,
        //   "message_id": 903872512,
        //   "new_content": {
        //     "@type": "messageText",
        //     "text": {
        //       "@type": "formattedText",
        //       "text": "Blabla",
        //       "entities": []
        //     },
        //     "web_page": {
        //       "@type": "webPage",
        //       "url": "https://eu-renaissance.org/fr/notre-projet",
        //       "display_url": "eu-renaissance.org/fr/notre-projet",
        //       "type": "photo",
        //       "site_name": "Renaissance",
        //       "title": "Notre projet | Renaissance",
        //       "description": "N'attendez pas une meilleure Europe. Changez-la !",
        //       "photo": {
        //         "@type": "photo",
        //         "has_stickers": false,
        //         "sizes": [
        //           {
        //             "@type": "photoSize",
        //             "type": "s",
        //             "photo": {
        //               "@type": "file",
        //               "id": 5,
        //               "size": 1379,
        //               "expected_size": 1379,
        //               "local": {
        //                 "@type": "localFile",
        //                 "path": "",
        //                 "can_be_downloaded": true,
        //                 "can_be_deleted": false,
        //                 "is_downloading_active": false,
        //                 "is_downloading_completed": false,
        //                 "download_offset": 0,
        //                 "downloaded_prefix_size": 0,
        //                 "downloaded_size": 0
        //               },
        //               "remote": {
        //                 "@type": "remoteFile",
        //                 "id": "AgADBAADtagxG7ENDVD8QQtTJw0uGoUvIhsABAlEAqT02_Ix3a4EAAEC",
        //                 "is_uploading_active": false,
        //                 "is_uploading_completed": true,
        //                 "uploaded_size": 1379
        //               }
        //             },
        //             "width": 90,
        //             "height": 47
        //           },
        //           ...
        //         ]
        //       },
        //       "embed_url": "",
        //       "embed_type": "",
        //       "embed_width": 0,
        //       "embed_height": 0,
        //       "duration": 0,
        //       "author": "",
        //       "instant_view_version": 0
        //     }
        //   }
        // }
        await setupMessageContent(update.new_content)
        sendDataToWebSocketClients(indexWebSocketServer, update)
        break
      case "updateMessageEdited":
        // {
        //   "@type": "updateMessageEdited",
        //   "chat_id": -1001346979214,
        //   "message_id": 903872512,
        //   "edit_date": 1557312568
        // }
        break
      case "updateMessageMentionRead":
        // {
        //   "@type": "updateMessageMentionRead",
        //   "chat_id": -332481416,
        //   "message_id": 58149830656,
        //   "unread_mention_count": 0
        // }
        break
      case "updateMessageViews":
        // {
        //   "@type": "updateMessageViews",
        //   "chat_id": -1001346979214,
        //   "message_id": 868220928,
        //   "views": 43
        // }
        break
      case "updateNewChat":
        // {
        //   "@type": "updateNewChat",
        //   "chat": {
        //     "@type": "chat",
        //     "id": -123456789,
        //     "type": {
        //       "@type": "chatTypeBasicGroup",
        //       "basic_group_id": 987654321
        //     },
        //     "title": "Groupe bidon",
        //     "photo": {
        //       "@type": "chatPhoto",
        //       "small": {
        //         "@type": "file",
        //         "id": 3,
        //         "size": 0,
        //         "expected_size": 0,
        //         "local": {
        //           "@type": "localFile",
        //           "path": "",
        //           "can_be_downloaded": true,
        //           "can_be_deleted": false,
        //           "is_downloading_active": false,
        //           "is_downloading_completed": false,
        //           "download_offset": 0,
        //           "downloaded_prefix_size": 0,
        //           "downloaded_size": 0
        //         },
        //         "remote": {
        //           "@type": "remoteFile",
        //           "id": "AQADBAAT0UedGgAEV7bKcoAhClAaNwcBBQI",
        //           "is_uploading_active": false,
        //           "is_uploading_completed": true,
        //           "uploaded_size": 0
        //         }
        //       },
        //       "big": {
        //         "@type": "file",
        //         "id": 4,
        //         "size": 0,
        //         "expected_size": 0,
        //         "local": {
        //           "@type": "localFile",
        //           "path": "",
        //           "can_be_downloaded": true,
        //           "can_be_deleted": false,
        //           "is_downloading_active": false,
        //           "is_downloading_completed": false,
        //           "download_offset": 0,
        //           "downloaded_prefix_size": 0,
        //           "downloaded_size": 0
        //         },
        //         "remote": {
        //           "@type": "remoteFile",
        //           "id": "AQADBAAT0UedGgAELOMtDlVt5XMcNwcBBQI",
        //           "is_uploading_active": false,
        //           "is_uploading_completed": true,
        //           "uploaded_size": 0
        //         }
        //       }
        //     },
        //     "order": "0",
        //     "is_pinned": false,
        //     "is_marked_as_unread": false,
        //     "is_sponsored": false,
        //     "can_be_deleted_only_for_self": true,
        //     "can_be_deleted_for_all_users": false,
        //     "can_be_reported": false,
        //     "default_disable_notification": false,
        //     "unread_count": 2,
        //     "last_read_inbox_message_id": 160432128,
        //     "last_read_outbox_message_id": 154140672,
        //     "unread_mention_count": 0,
        //     "notification_settings": {
        //       "@type": "chatNotificationSettings",
        //       "use_default_mute_for": true,
        //       "mute_for": 0,
        //       "use_default_sound": true,
        //       "sound": "default",
        //       "use_default_show_preview": true,
        //       "show_preview": false,
        //       "use_default_disable_pinned_message_notifications": true,
        //       "disable_pinned_message_notifications": false,
        //       "use_default_disable_mention_notifications": true,
        //       "disable_mention_notifications": false
        //     },
        //     "pinned_message_id": 0,
        //     "reply_markup_message_id": 0,
        //     "client_data": ""
        //   }
        // }
        break
      case "updateNewMessage":
        await setupMessageContent(update.message.content)
        sendDataToWebSocketClients(indexWebSocketServer, update)
        break
      case "updateOption":
        // {
        //   "@type": "updateOption",
        //   "name": "call_connect_timeout_ms",
        //   "value": {
        //     "@type": "optionValueInteger",
        //     "value": 30000
        //   }
        // }
        break
      case "updateScopeNotificationSettings":
        // {
        //   "@type": "updateScopeNotificationSettings",
        //   "scope": {
        //     "@type": "notificationSettingsScopePrivateChats"
        //   },
        //   "notification_settings": {
        //     "@type": "scopeNotificationSettings",
        //     "mute_for": 0,
        //     "sound": "default",
        //     "show_preview": true,
        //     "disable_pinned_message_notifications": false,
        //     "disable_mention_notifications": false
        //   }
        // }
        break
      case "updateSupergroup":
        // {
        //   "@type": "updateSupergroup",
        //   "supergroup": {
        //     "@type": "supergroup",
        //     "id": 1103318029,
        //     "username": "",
        //     "date": 1496931520,
        //     "status": {
        //       "@type": "chatMemberStatusMember"
        //     },
        //     "member_count": 0,
        //     "anyone_can_invite": true,
        //     "sign_messages": true,
        //     "is_channel": false,
        //     "is_verified": false,
        //     "restriction_reason": ""
        //   }
        // }
        break
      case "updateSupergroupFullInfo":
        // {
        //   "@type": "updateSupergroupFullInfo",
        //   "supergroup_id": 1346979214,
        //   "supergroup_full_info": {
        //     "@type": "supergroupFullInfo",
        //     "description": "",
        //     "member_count": 669,
        //     "administrator_count": 0,
        //     "restricted_count": 0,
        //     "banned_count": 0,
        //     "can_get_members": false,
        //     "can_set_username": false,
        //     "can_set_sticker_set": false,
        //     "can_view_statistics": false,
        //     "is_all_history_available": true,
        //     "sticker_set_id": "0",
        //     "invite_link": "",
        //     "upgraded_from_basic_group_id": 0,
        //     "upgraded_from_max_message_id": 0
        //   }
        // }
        break
      case "updateUnreadChatCount":
        // {
        //   "@type": "updateUnreadChatCount",
        //   "unread_count": 2,
        //   "unread_unmuted_count": 2,
        //   "marked_as_unread_count": 0,
        //   "marked_as_unread_unmuted_count": 0
        // }
        break
      case "updateUnreadMessageCount":
        // {
        //   "@type": "updateUnreadMessageCount",
        //   "unread_count": 4,
        //   "unread_unmuted_count": 4
        // }
        break
      case "updateUser":
        // {
        //   "@type": "updateUser",
        //   "user": {
        //     "@type": "user",
        //     "id": 123456789,
        //     "first_name": "John",
        //     "last_name": "Doe",
        //     "username": "",
        //     "phone_number": "33601234567",
        //     "status": {
        //       "@type": "userStatusOffline",
        //       "was_online": 1556989076
        //     },
        //     "profile_photo": {
        //       "@type": "profilePhoto",
        //       "id": "1243261443240224245",
        //       "small": {
        //         "@type": "file",
        //         "id": 1,
        //         "size": 0,
        //         "expected_size": 0,
        //         "local": {
        //           "@type": "localFile",
        //           "path": "",
        //           "can_be_downloaded": true,
        //           "can_be_deleted": false,
        //           "is_downloading_active": false,
        //           "is_downloading_completed": false,
        //           "download_offset": 0,
        //           "downloaded_prefix_size": 0,
        //           "downloaded_size": 0
        //         },
        //         "remote": {
        //           "@type": "remoteFile",
        //           "id": "AQADBABD9acxG7LzQBEACI9RwxoABFxSStlNe7u8rxAGAAEC",
        //           "is_uploading_active": false,
        //           "is_uploading_completed": true,
        //           "uploaded_size": 0
        //         }
        //       },
        //       "big": {
        //         "@type": "file",
        //         "id": 2,
        //         "size": 0,
        //         "expected_size": 0,
        //         "local": {
        //           "@type": "localFile",
        //           "path": "",
        //           "can_be_downloaded": true,
        //           "can_be_deleted": false,
        //           "is_downloading_active": false,
        //           "is_downloading_completed": false,
        //           "download_offset": 0,
        //           "downloaded_prefix_size": 0,
        //           "downloaded_size": 0
        //         },
        //         "remote": {
        //           "@type": "remoteFile",
        //           "id": "AQADBABD9acxG7LzQBEACI9RwxoABFgzN75NQ4lFsRAGAAEC",
        //           "is_uploading_active": false,
        //           "is_uploading_completed": true,
        //           "uploaded_size": 0
        //         }
        //       }
        //     },
        //     "outgoing_link": {
        //       "@type": "linkStateIsContact"
        //     },
        //     "incoming_link": {
        //       "@type": "linkStateNone"
        //     },
        //     "is_verified": false,
        //     "is_support": false,
        //     "restriction_reason": "",
        //     "have_access": true,
        //     "type": {
        //       "@type": "userTypeRegular"
        //     },
        //     "language_code": ""
        //   }
        // }
        break
      case "updateUserChatAction":
        // {
        //   "@type": "updateUserChatAction",
        //   "chat_id": -332481416,
        //   "user_id": 289469362,
        //   "action": {
        //     "@type": "chatActionTyping"
        //   }
        // }
        break
      case "updateUserFullInfo":
        break
      case "updateUserStatus":
        // {
        //   "@type": "updateUserStatus",
        //   "user_id": 123456789,
        //   "status": {
        //     "@type": "userStatusOnline",
        //     "expires": 1556991456
        //   }
        // }
        break
      default:
        console.log("[update]", JSON.stringify(update, null, 2))
        break
    }
  })
  telegramClient.registerCallback("td:error", error => {
    console.log("[error]", error)
  })
  // Save tglib default handler which prompt input at console
  const telegramInputDefaultHandler = telegramClient.callbacks["td:getInput"]
  // Register own callback for returning auth details
  telegramClient.registerCallback("td:getInput", async args => {
    if (args.string === "tglib.input.AuthorizationType") {
      return "user"
    } else if (args.string === "tglib.input.AuthorizationValue") {
      return telegram.phoneNumber
    } else if (
      args.string === "tglib.input.AuthorizationCode" &&
      telegram.authorizationCode !== null
    ) {
      return telegram.authorizationCode
    }
    return await telegramInputDefaultHandler(args)
  })

  await telegramClient.ready
}
