-- Change the primary key of amendements from (amdt_id) to (amdt_texte_id, amdt_id)
ALTER TABLE amendements DROP CONSTRAINT amendements_pkey CASCADE;
ALTER TABLE amendements ADD CONSTRAINT amendements_pkey PRIMARY KEY (amdt_texte_id, amdt_id);

-- Adapt cosignatures to this change by adding the column cosig_texte_id and recreating the foreign key
ALTER TABLE cosignatures ADD COLUMN cosig_texte_id bigint;
UPDATE cosignatures SET cosig_texte_id = (SELECT amdt_texte_id FROM amendements WHERE cosig_amdt_id = amdt_id);
ALTER TABLE cosignatures ALTER COLUMN cosig_texte_id SET NOT NULL;
ALTER TABLE cosignatures ADD CONSTRAINT cosignatures_cosig_texte_id_cosig_amdt_id_fkey FOREIGN KEY (cosig_texte_id, cosig_amdt_id) REFERENCES amendements(amdt_texte_id, amdt_id);

-- Add a constraint on cosignatures to avoid multiple supports for a given author on a given amendment
ALTER TABLE cosignatures ADD CONSTRAINT cosignatures_cosig_texte_id_cosig_amdt_id_cosig_auteur_key UNIQUE (cosig_texte_id, cosig_amdt_id, cosig_auteur);
