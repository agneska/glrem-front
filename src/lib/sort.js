const regex_article_name = /^(\d+)(?: er)?(?: (([A-Z]+)|((?:un(?:de?)?|duo(?:de)?|ter|quater|quin|sex?|sept|octo|novo)?(?:dec|v[ei]c|tr[ei]c|quadrag|quinquag|sexag|septuag|octog|nonag)ies|semel|bis|ter|quater|(?:quinqu|sex|sept|oct|no[nv])ies)))?$/

const pre_alinea_order = {
  AVANT: 0,
  SUPPRESSION: 1,
  REDACTION_GLOBALE: 2,
  A: 3,
  APRES: 4,
}

// TODO improve
const bister = {
  semel: 1,
  bis: 2,
  ter: 3,
  quater: 4,
  cinquies: 5,
  sexies: 6,
  septies: 7,
  octies: 8,
  nonies: 9,
  decies: 10,
  undecies: 11,
  duodecies: 12,
  tridecies: 13,
  quaterdecies: 14,
  quindecies: 15,
  sexdecies: 16,
  septdecies: 17,
  octodecies: 18,
  duodevicies: 18,
  duodevecies: 18,
  novodecies: 19,
  undevicies: 19,
  undevecies: 19,
  vicies: 20,
  vecies: 20,
  unvicies: 21,
  unvecies: 21,
  duovicies: 22,
  duovecies: 22,
  tervicies: 23,
  tervecies: 23,
  quatervicies: 24,
  quatervecies: 24,
  quinvecies: 25,
  quinvicies: 25,
  sevecies: 25,
  sevicies: 25,
  sexvecies: 25,
  sexvicies: 25,
  septvecies: 27,
  septvicies: 27,
  octovecies: 28,
  octovicies: 28,
  duodetricies: 28,
  duodetrecies: 28,
  novovecies: 29,
  novovicies: 29,
  undetricies: 29,
  undetrecies: 29,
  tricies: 30,
  trecies: 30,
  untricies: 31,
  untrecies: 31,
  duotricies: 32,
  duotrecies: 32,
  tertricies: 33,
  tertrecies: 33,
  quatertricies: 34,
  quatertrecies: 34,
  quintrecies: 35,
  quintricies: 35,
  setrecies: 35,
  setricies: 35,
  sextrecies: 35,
  sextricies: 35,
  septtrecies: 37,
  septtricies: 37,
  octotrecies: 38,
  octotricies: 38,
  duodequadracies: 38,
  novotrecies: 39,
  undequadracies: 39,
  novotricies: 39,
  quadracies: 40,
}

/**
 * Sort two amendments.
 *
 * This is a composite order: there are priority between different fields:
 * 1. articles numbers (see sub-function)
 * 2. additional article/amendment type (see constant pre_alinea_order)
 * 3. alineas numbers
 * 4. additional alinea
 *
 * This sort is guaranteed to be stable (thanks to the linear id of the amendments).
 * Note that if an article name is unrecognised, it will be sorted as "before".
 *
 * @param Dict amdt_a First amendment, dictionary containing the keys: division, division_place, alinea, alinea_place
 * @param Dict amdt_b Second amendment, dictionary containing the keys: division, division_place, alinea, alinea_place
 * @return int 0, -1, 1 if first amendment is respectively equal, before, or after the second amendment
 */
function sort_amendements(amdt_a, amdt_b) {
  // Zeroth step: same amendment
  if (amdt_a.amdt_id === amdt_b.amdt_id) {
    return 0
  }

  // First step: sort by articles numbers
  const relative_order_articles = sort_articles(
    amdt_a.amdt_division,
    amdt_b.amdt_division,
  )
  if (relative_order_articles !== 0) {
    return relative_order_articles
  }

  // Second step: sort by additional article/amendment type
  const pre_alinea_order_a = pre_alinea_order[amdt_a.amdt_division_place],
    pre_alinea_order_b = pre_alinea_order[amdt_b.amdt_division_place]
  if (pre_alinea_order_a !== pre_alinea_order_b) {
    return pre_alinea_order_a < pre_alinea_order_b ? -1 : 1
  }

  // Third step: sort by alineas numbers
  const alinea_a = Number(amdt_a.amdt_alinea),
    alinea_b = Number(amdt_b.amdt_alinea)
  if (alinea_a && alinea_b && alinea_a !== alinea_b) {
    return alinea_a < alinea_b ? -1 : 1
  }

  // Fourth step: sort by additional alinea
  const alinea_order_a = pre_alinea_order[amdt_a.amdt_alinea_place],
    alinea_order_b = pre_alinea_order[amdt_b.amdt_alinea_place]
  if (alinea_a && alinea_b && alinea_order_a !== alinea_order_b) {
    return alinea_order_a < alinea_order_b ? -1 : 1
  }

  // Fifth step: organisational sort
  if (amdt_a.amdt_ordre !== amdt_b.amdt_ordre) {
    return Number(amdt_a.amdt_ordre) < Number(amdt_b.amdt_ordre) ? -1 : 1
  }

  return Number(amdt_a.amdt_id) < Number(amdt_b.amdt_id) ? -1 : 1
}

/**
 * Sort two articles names.
 *
 * Note that if an article name is unrecognised, it will be sorted as "before".
 *
 * @param string article_a First article name.
 * @param string article_b Second article name.
 * @return int 0, -1, 1 if first article is respectively equal, before, or after the second article
 */
function sort_articles(article_a, article_b) {
  article_a = article_a.trim()
  article_b = article_b.trim()

  // Trivial case
  if (article_a === article_b) {
    return 0
  }

  // Some first sanitisation
  article_a = article_a.replace(/^Article +/, "")
  article_b = article_b.replace(/^Article +/, "")

  const parts_a = regex_article_name.exec(article_a),
    parts_b = regex_article_name.exec(article_b)

  // In case of unrecognised article name
  if (parts_a === null || parts_b === null) {
    // Check special names
    if (/^État [A-Z]$/.exec(article_a) || /^État [A-Z]$/.exec(article_b)) {
      if (/^État [A-Z]$/.exec(article_a) && /^État [A-Z]$/.exec(article_b)) {
        return article_a < article_b ? -1 : 1
      } else if (/^État [A-Z]$/.exec(article_a)) {
        return 1
      }
      return -1
    }
    if (article_a === "liminaire" || article_b === "liminaire") {
      return article_a === "liminaire" ? -1 : 1
    }
    // TODO add annexe|ex[ée]cution|unique|(pr[ée])?liminaire|pr[ée]ambule
    return parts_a === parts_b ? 0 : parts_a === null ? -1 : 1
  }

  const nb_a = Number(parts_a[1]),
    nb_b = Number(parts_b[1]),
    compl_a = parts_a[2],
    compl_b = parts_b[2],
    pre_a = parts_a[3],
    pre_b = parts_b[3],
    bister_a = parts_a[4],
    bister_b = parts_b[4]

  // Simple articles numbers
  if (nb_a !== nb_b) {
    return nb_a < nb_b ? -1 : 1
  }

  // One is simple and the other is A or bis
  if (compl_a === undefined && compl_b) {
    return pre_b ? 1 : -1
  } else if (compl_b === undefined && compl_a) {
    return pre_a ? -1 : 1
  } else if (pre_a && bister_b) {
    return -1
  } else if (bister_a && pre_b) {
    return 1
  }
  if (bister_a && bister_b) {
    if (!bister[bister_a] || !bister[bister_b]) {
      return bister[bister_a] === bister[bister_b]
        ? 0
        : bister[bister_a]
        ? -1
        : 1
    }
    return bister[bister_a] < bister[bister_b] ? -1 : 1
  }
  if (pre_a && pre_b) {
    const min = pre_a.length < pre_b.length ? pre_a.length : pre_b.length
    for (let i = 0; i < min; i++) {
      if (pre_a[i] !== pre_b[i]) {
        return pre_a[i] < pre_b[i] ? -1 : 1
      }
    }
    return pre_a.length === min ? 1 : -1
  }

  // TODO: manage: bis A and recursively

  return 0
}

/**
 * Sort randomly.
 *
 * @param any a First object.
 * @param any b Second object.
 * @return int 0, -1, 1
 */
function random() {
  return Math.random() < 0.5 ? -1 : 1
}

export default { sort_amendements, sort_articles, pre_alinea_order, random }

// vim: set ts=2 sw=2 sts=2 et:
