import assert from "assert"
import dedent from "dedent-js"
import fastXmlParser from "fast-xml-parser"
import fetch from "node-fetch"
import querystring from "querystring"
import url from "url"

import { db } from "../database"
import { slugify } from "../lib/strings"
import serverConfig from "../server_config"

const allCommissionNames = [
  "Affaires culturelles et éducation",
  "Affaires économiques",
  "Affaires étrangères",
  "Affaires sociales",
  "Défense",
  "Développement durable",
  "Finances",
  "Lois",
]
const allGroupNames = [
  "Attachés parlementaires LaREM",
  "Collaborateurs groupe LaREM",
  "Députés LaREM",
]
const assembleeName = "Assemblée"
const groupePolitiqueName = "LaREM"
const { nextcloud } = serverConfig

export async function addUserToGroup(groupName, user) {
  const response = await fetch(
    url.resolve(
      nextcloud.url,
      `ocs/v1.php/cloud/users/${encodeURIComponent(user.samlId)}/groups`,
    ),
    {
      body: querystring.stringify({
        groupid: groupName,
      }),
      headers: {
        Authorization:
          "Basic " +
          Buffer.from(nextcloud.user + ":" + nextcloud.password).toString(
            "base64",
          ),
        "Content-Type": "application/x-www-form-urlencoded",
        "OCS-APIRequest": "true",
        "User-Agent": nextcloud.userAgent,
      },
      method: "POST",
    },
  )
  const text = await response.text()
  assert(fastXmlParser.validate(text))
  const xmlOptions = {
    ignoreNameSpace: true,
  }
  const result = fastXmlParser.parse(text, xmlOptions)
  const { ocs } = result
  assert.strictEqual(ocs.meta.statuscode, 100, text)
}

export async function createUser(user, groupNames) {
  if (user.nextcloudPassword === null) {
    // A password is required to be able to create a Nextcloud user without email.
    user.nextcloudPassword = Math.random()
      .toString(36)
      .slice(-8)
    await db.none(
      `
        UPDATE users
        SET
          user_nextcloud_password = $<nextcloudPassword>
        WHERE user_name_id = $<nameId>
      `,
      user,
    )
  }
  const response = await fetch(
    url.resolve(nextcloud.url, "ocs/v1.php/cloud/users"),
    {
      body: querystring.stringify({
        displayName: user.fullName,
        // CAUTION: If email is given, a Welcome email is always sent when user is created.
        // email,
        "groups[]": groupNames,
        language: "fr",
        password: user.nextcloudPassword,
        quota: "20 MB",
        // "subadmin[]": [],
        userid: user.samlId,
      }),
      headers: {
        Authorization:
          "Basic " +
          Buffer.from(nextcloud.user + ":" + nextcloud.password).toString(
            "base64",
          ),
        "Content-Type": "application/x-www-form-urlencoded",
        "OCS-APIRequest": "true",
        "User-Agent": nextcloud.userAgent,
      },
      method: "POST",
    },
  )
  const text = await response.text()
  assert(fastXmlParser.validate(text))
  const xmlOptions = {
    ignoreNameSpace: true,
  }
  const result = fastXmlParser.parse(text, xmlOptions)
  const { ocs } = result
  assert.strictEqual(ocs.meta.statuscode, 100, text)
}

function escapeXml(s) {
  return s
    .replace(/&apos;/g, "'")
    .replace(/&quot;/g, '"')
    .replace(/&gt;/g, ">")
    .replace(/&lt;/g, "<")
    .replace(/&amp;/g, "&")
}

export async function removeUserFromGroup(groupName, user) {
  const response = await fetch(
    url.resolve(
      nextcloud.url,
      `ocs/v1.php/cloud/users/${encodeURIComponent(user.samlId)}/groups`,
    ),
    {
      body: querystring.stringify({
        groupid: groupName,
      }),
      headers: {
        Authorization:
          "Basic " +
          Buffer.from(nextcloud.user + ":" + nextcloud.password).toString(
            "base64",
          ),
        "Content-Type": "application/x-www-form-urlencoded",
        "OCS-APIRequest": "true",
        "User-Agent": nextcloud.userAgent,
      },
      method: "DELETE",
    },
  )
  const text = await response.text()
  assert(fastXmlParser.validate(text))
  const xmlOptions = {
    ignoreNameSpace: true,
  }
  const result = fastXmlParser.parse(text, xmlOptions)
  const { ocs } = result
  assert.strictEqual(ocs.meta.statuscode, 100, text)
}

export async function retrieveGroupUsernames(groupName) {
  const response = await fetch(
    url.resolve(
      nextcloud.url,
      `ocs/v1.php/cloud/groups/${encodeURIComponent(groupName)}`,
    ),
    {
      headers: {
        Authorization:
          "Basic " +
          Buffer.from(nextcloud.user + ":" + nextcloud.password).toString(
            "base64",
          ),
        "OCS-APIRequest": "true",
        "User-Agent": nextcloud.userAgent,
      },
    },
  )
  const text = await response.text()
  assert(fastXmlParser.validate(text))
  const xmlOptions = {
    ignoreNameSpace: true,
  }
  const result = fastXmlParser.parse(text, xmlOptions)
  const { ocs } = result
  assert.strictEqual(ocs.meta.statuscode, 100, text)
  let usernames = ocs.data.users.element
  if (!usernames) {
    usernames = []
  } else if (!Array.isArray(usernames)) {
    assert.strictEqual(typeof usernames, "string")
    usernames = [usernames]
  }
  return usernames
}

export async function retrieveUserCalendars(user) {
  const response = await fetch(
    url.resolve(
      nextcloud.url,
      `remote.php/dav/calendars/${encodeURIComponent(user.samlId)}/`,
    ),
    {
      body: dedent`
        <d:propfind
          xmlns:c="urn:ietf:params:xml:ns:caldav"
          xmlns:cs="http://calendarserver.org/ns/"
          xmlns:d="DAV:"
        >
          <d:prop>
            <c:calendar-description />
            <c:calendar-timezone />
            <d:displayname />
            <cs:getctag />
            <d:resourcetype />
            <c:supported-calendar-component-set />
            <d:sync-token />
          </d:prop>
        </d:propfind>
      `,
      headers: {
        Authorization:
          "Basic " +
          Buffer.from(user.samlId + ":" + user.nextcloudPassword).toString(
            "base64",
          ),
        "Content-Type": "Content-Type: application/xml; charset=utf-8",
        "OCS-APIRequest": "true",
        "User-Agent": nextcloud.userAgent,
      },
      method: "PROPFIND",
    },
  )
  const text = await response.text()
  if (response.status !== 207) {
    console.error(response.status, response.statusText)
    console.error(text)
    throw new Error("Request failed")
  }
  assert(fastXmlParser.validate(text))
  const xmlOptions = {
    ignoreNameSpace: true,
  }
  const result = fastXmlParser.parse(text, xmlOptions)
  // console.log(JSON.stringify(result, null, 2))
  let responses = result.multistatus.response
  if (!responses) {
    responses = []
  } else if (!Array.isArray(responses)) {
    assert(responses)
    responses = [responses]
  }
  const calendars = []
  for (let response of responses) {
    let propstatElements = response.propstat
    if (!propstatElements) {
      propstatElements = response.propstat = []
    } else if (!Array.isArray(propstatElements)) {
      assert(propstatElements)
      propstatElements = response.propstat = [propstatElements]
    }
    for (let propstat of propstatElements) {
      if (propstat.status === "HTTP/1.1 200 OK") {
        const isCalendar = propstat.prop.resourcetype.calendar !== undefined
        if (isCalendar) {
          const calendarUrl = url.resolve(nextcloud.url, response.href)
          const splitUrl = calendarUrl.split("/")
          const segment = splitUrl[splitUrl.length - 2]
          calendars.push({
            displayName: propstat.prop.displayname,
            segment,
            url: calendarUrl,
          })
        }
      } else {
        assert.strictEqual(propstat.status, "HTTP/1.1 404 Not Found")
      }
    }
  }
  return calendars
}

export async function retrieveUsernames() {
  const response = await fetch(
    url.resolve(nextcloud.url, "ocs/v1.php/cloud/users"),
    {
      headers: {
        Authorization:
          "Basic " +
          Buffer.from(nextcloud.user + ":" + nextcloud.password).toString(
            "base64",
          ),
        "OCS-APIRequest": "true",
        "User-Agent": nextcloud.userAgent,
      },
    },
  )
  const text = await response.text()
  assert(fastXmlParser.validate(text))
  const xmlOptions = {
    ignoreNameSpace: true,
  }
  const result = fastXmlParser.parse(text, xmlOptions)
  const { ocs } = result
  assert.strictEqual(ocs.meta.statuscode, 100, text)
  let usernames = ocs.data.users.element
  if (!usernames) {
    usernames = []
  } else if (!Array.isArray(usernames)) {
    assert.strictEqual(typeof usernames, "string")
    usernames = [usernames]
  }
  return usernames
}

export async function shareCalendar(calendar, user) {
  // cf:
  // * http://sabre.io/dav/caldav-sharing/
  // * https://tools.ietf.org/html/draft-pot-webdav-resource-sharing-03
  // for explainations of some parts of the XML.
  const response = await fetch(calendar.url, {
    body: dedent`
        <o:share xmlns:o="http://owncloud.org/ns" xmlns:d="DAV:">
          <o:set>
            <d:href>principal:principals/users/${escapeXml(
              user.samlId,
            )}</d:href>
            <o:summary>${escapeXml(calendar.displayName)} shared by ${escapeXml(
              nextcloud.user,
            )}</o:summary>
          </o:set>
        </o:share>
      `,
    headers: {
      Authorization:
        "Basic " +
        Buffer.from(nextcloud.user + ":" + nextcloud.password).toString(
          "base64",
        ),
      "Content-Type": "Content-Type: application/xml; charset=utf-8",
      "OCS-APIRequest": "true",
      "User-Agent": nextcloud.userAgent,
    },
    method: "POST",
  })
  if (!response.ok) {
    const text = await response.text()
    console.log(text)
    assert(response.ok)
  }
  // The response has no body.
}

export async function upsertUser(user) {
  console.log(`Nextcloud: Upserting ${user.samlId}…`)
  const userGroupNames = []
  if (
    ["COLLABORATEUR_GROUPE", "STAGIAIRE_GROUPE", "SUPPORT"].includes(user.role)
  ) {
    userGroupNames.push("Collaborateurs groupe LaREM")
  }
  if (
    ["COLLABORATEUR_DEPUTE", "SUPPORT"].includes(user.role)
  ) {
    userGroupNames.push("Attachés parlementaires LaREM")
  }
  if (["DEPUTE", "SUPPORT"].includes(user.role)) {
    userGroupNames.push("Députés LaREM")
  }

  const existingUsernames = new Set(await retrieveUsernames())
  if (existingUsernames.has(user.samlId)) {
    // User already has a Nextcloud account.
    await upsertUserPassword(user)
    for (let groupName of allGroupNames) {
      const groupUsernames = new Set(await retrieveGroupUsernames(groupName))
      if (userGroupNames.includes(groupName)) {
        if (!groupUsernames.has(user.samlId)) {
          console.log(
            `Nextcloud: Adding ${user.samlId} to group "${groupName}"…`,
          )
          await addUserToGroup(groupName, user)
        }
      } else {
        if (groupUsernames.has(user.samlId)) {
          console.log(
            `Nextcloud: Removing ${user.samlId} from group "${groupName}"…`,
          )
          await removeUserFromGroup(groupName, user)
        }
      }
    }
  } else {
    // Create Nextcloud user.
    console.log(`Nextcloud: Creating user ${user.samlId}…`)
    await createUser(user, userGroupNames)
  }

  const userCalendarSegments = new Set([slugify(assembleeName)])
  if (
    ["COLLABORATEUR_GROUPE", "STAGIAIRE_GROUPE", "SUPPORT"].includes(user.role)
  ) {
    for (const commissionName of allCommissionNames) {
      userCalendarSegments.add(slugify(commissionName))
    }
    userCalendarSegments.add(slugify(groupePolitiqueName))
  }
  if (["COLLABORATEUR_DEPUTE", "DEPUTE", "SUPPORT"].includes(user.role) && user.idTribun !== null) {
    const auteur = await db.oneOrNone(
      `
        SELECT *
        FROM auteurs
        WHERE aut_id = $<id>
      `,
      {
        id: `PA${user.idTribun}`,
      },
    )
    console.log("auteur", auteur)
    if (auteur !== null) {
      for (const annuaireDisplayName of auteur.aut_annuaires || []) {
        userCalendarSegments.add(slugify(annuaireDisplayName))
      }
    }
  }

  console.log(`Loading ${user.samlId} calendars…`)
  const userExistingCalendars = await retrieveUserCalendars(user)
  const userExistingCalendarSegments = new Set(
    // Remove "_shared_by_xxx" suffix.
    userExistingCalendars.map(calendar => calendar.segment.split("_")[0]),
  )
  console.log(
    `  Existing calendars for ${user.samlId}: ${[
      ...userExistingCalendarSegments,
    ]}`,
  )
  const calendars = await retrieveUserCalendars({
    samlId: nextcloud.user,
    nextcloudPassword: nextcloud.password,
  })
  const calendarBySegment = calendars.reduce((calendarBySegment, calendar) => {
    calendarBySegment[calendar.segment] = calendar
    return calendarBySegment
  }, {})
  for (let calendarSegment of userCalendarSegments) {
    if (userExistingCalendarSegments.has(calendarSegment)) {
      // User is already subscribed to this calendar.
      continue
    }
    const calendar = calendarBySegment[calendarSegment]
    if (calendar === undefined) {
      console.log(
        `  Calendar ${calendarSegment} can't be shared because it doesn't exist.`,
      )
      continue
    }
    console.log(
      `  Sharing calendar with ${user.samlId}: "${calendar.displayName}" at ${
        calendar.url
      }…`,
    )
    await shareCalendar(calendar, user)
  }

  console.log(`  Nextcloud: Upserted ${user.samlId}.`)
}

export async function upsertUserPassword(user) {
  // Update user password if the one currently stored in database is missing or doesn't work.
  let response
  if (user.nextcloudPassword !== null) {
    response = await fetch(
      url.resolve(
        nextcloud.url,
        `ocs/v1.php/cloud/users/${encodeURIComponent(user.samlId)}`,
      ),
      {
        headers: {
          Authorization:
            "Basic " +
            Buffer.from(user.samlId + ":" + user.nextcloudPassword).toString(
              "base64",
            ),
          "Content-Type": "application/x-www-form-urlencoded",
          "OCS-APIRequest": "true",
          "User-Agent": nextcloud.userAgent,
        },
      },
    )
  }
  if (user.nextcloudPassword === null || response.status === 401) {
    // Missing or wrong password => Create a new one.
    user.nextcloudPassword = Math.random()
      .toString(36)
      .slice(-8)
    await db.none(
      `
        UPDATE users
        SET
          user_nextcloud_password = $<nextcloudPassword>
        WHERE user_name_id = $<nameId>
      `,
      user,
    )

    response = await fetch(
      url.resolve(
        nextcloud.url,
        `ocs/v1.php/cloud/users/${encodeURIComponent(user.samlId)}`,
      ),
      {
        body: querystring.stringify({
          key: "password",
          value: user.nextcloudPassword,
        }),
        headers: {
          Authorization:
            "Basic " +
            Buffer.from(nextcloud.user + ":" + nextcloud.password).toString(
              "base64",
            ),
          "Content-Type": "application/x-www-form-urlencoded",
          "OCS-APIRequest": "true",
          "User-Agent": nextcloud.userAgent,
        },
        method: "PUT",
      },
    )
  }
  const text = await response.text()
  assert(fastXmlParser.validate(text))
  const xmlOptions = {
    ignoreNameSpace: true,
  }
  const result = fastXmlParser.parse(text, xmlOptions)
  const { ocs } = result
  assert.strictEqual(ocs.meta.statuscode, 100, text)
}
