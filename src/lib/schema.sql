-- Table système pour enregistrer la version du schéma
CREATE TABLE IF NOT EXISTS version (
  v_number integer NOT NULL
);

-- Rôle (fonction) d'un utilisateur
CREATE TYPE user_role AS ENUM (
  'COLLABORATEUR_DEPUTE',
  'COLLABORATEUR_GROUPE',
  'DEPUTE',
  'STAGIAIRE_DEPUTE',
  'STAGIAIRE_GROUPE',
  'SUPPORT'
);
COMMENT ON TYPE user_role IS 'rôle (fonction) d''un utilisateur';

-- Utilisateurs connectés
CREATE TABLE IF NOT EXISTS users (
  user_name_id text NOT NULL PRIMARY KEY,
  user_full_name text NOT NULL,
  user_member_of text[],
  user_id_tribun text,
  user_division text,
  user_manager text,
  user_saml_id text NOT NULL UNIQUE,
  user_email text UNIQUE,
  user_role user_role,
  user_nextcloud_password text
);
COMMENT ON TABLE users IS 'utilisateurs pouvant s''identifier sur l''application';
COMMENT ON COLUMN users.user_name_id IS 'identifiant unique utilisé pour s''identifier sur le site de l''Assemblée';
COMMENT ON COLUMN users.user_full_name IS 'prénom et nom';
COMMENT ON COLUMN users.user_member_of IS 'tableau des groupes LDAP auquel appartient l''utilisateur';
COMMENT ON COLUMN users.user_id_tribun IS 'code d''un député à l''Assemblée';
COMMENT ON COLUMN users.user_division IS 'nom de la circonscription d''un député ou "Stagiaire" ou "Stagiaire Groupe"';
COMMENT ON COLUMN users.user_manager IS 'indentifiant LDAP du député d''un assistant parlementaire';
COMMENT ON COLUMN users.user_saml_id IS '(pseudo-)email constitué à partir du name_id et de "@assemblee-nationale.fr"';
COMMENT ON COLUMN users.user_email IS 'courriel (parfois absent) de l''utilisateur';
COMMENT ON COLUMN users.user_role IS 'rôle (fonction) de l''utilisateur';
COMMENT ON COLUMN users.user_nextcloud_password IS 'mot de passe Nextcloud (local) de l''utilisateur (changé après chaque identification SAML)';


-- Utilisateurs pouvant prendre l'identité d'un autre utilisateur
CREATE TABLE IF NOT EXISTS sudoers (
  sudo_name_id text NOT NULL PRIMARY KEY,
  sudo_id_tribun text,
  sudo_role user_role
);
COMMENT ON TABLE sudoers IS 'utilisateurs pouvant prendre l''identité d''un autre utilisateur';
COMMENT ON COLUMN sudoers.sudo_name_id IS 'identifiant unique utilisé pour s''identifier sur le site de l''Assemblée';
COMMENT ON COLUMN sudoers.sudo_id_tribun IS 'code d''un député à l''Assemblée à incarner';
COMMENT ON COLUMN sudoers.sudo_role IS 'rôle (fonction) de l''utilisateur à incarner';

-- Textes (métadonnées uniquement)
CREATE TABLE IF NOT EXISTS textes(
  txt_id text NOT NULL PRIMARY KEY, -- identifiant du texte
  txt_ordre integer,                -- ordre relatif du texte
  txt_titre text,                   -- titre long du texte
  txt_titre_court text,             -- titre court du texte
  txt_responsables text[],          -- responsables internes du texte
  txt_etape text,                   -- étape du texte (1re/2e/etc lecture)
  txt_commission text,              -- commission au fond saisie
  txt_date_depot timestamptz,       -- date de dépôt (création) de ce texte
  txt_officiel boolean,             -- le texte est officiel au sens "publié par l’Assemblée", au contraire d’un texte non-encore-publié par l’Assemblée pour des questions d’urgence à déposer des amendements
  txt_dossier text,                 -- identifiant du dossier législatif Assemblée
  txt_note text,                    -- note introductive courte
  txt_url_note text                 -- URL vers un document explicatif plus long
);

-- Plan des textes
CREATE TABLE IF NOT EXISTS plans(
  plan_id bigserial NOT NULL PRIMARY KEY, -- identifiant unique interne à la table
  plan_txt text NOT NULL                  -- identifiant du texte dans la table textes
    REFERENCES textes(txt_id),
  plan_division text NOT NULL,            -- nom de la division (article ou division supra-article)
  plan_division_parent bigint             -- identifiant de la division parente dans la table plans
    REFERENCES plans(plan_id),
  plan_alineas int                        -- nombre d’alinéas (dans les articles)
);

-- Auteurs au sens ELOI (députés)
CREATE TABLE IF NOT EXISTS auteurs (
  aut_id text PRIMARY KEY,
  aut_auth_id text,
  aut_civ text,
  aut_nom text NOT NULL,
  aut_prenom text,
  aut_alpha text NOT NULL,
  aut_trigramme text,
  aut_annuaires text[],
  aut_actif boolean
);
COMMENT ON TABLE auteurs IS 'auteurs au sens d''Eloi (députés)';
COMMENT ON COLUMN auteurs.aut_id IS 'identifiant unique Assemblée de la forme PA\d+ (PA = PArlementaire)';
COMMENT ON COLUMN auteurs.aut_auth_id IS 'identifiant correspondant au compte externe Assemblée';
COMMENT ON COLUMN auteurs.aut_civ IS 'civilité (M., Mme)';
COMMENT ON COLUMN auteurs.aut_nom IS 'nom civil';
COMMENT ON COLUMN auteurs.aut_prenom IS 'prénom civil';
COMMENT ON COLUMN auteurs.aut_alpha IS 'clé pour le tri alphabétique';
COMMENT ON COLUMN auteurs.aut_trigramme IS 'trigramme';
COMMENT ON COLUMN auteurs.aut_annuaires IS 'noms (aka "display names") des annuaires propres au député';
COMMENT ON COLUMN auteurs.aut_actif IS 'le député est-il en activité ?';

-- Type "statut de la commission" : au fond ou pour avis (utiliser NULL pour la séance)
CREATE TYPE statut_commission AS ENUM ('FOND', 'AVIS');

-- Textes amendables
CREATE TABLE IF NOT EXISTS textes_amendables(
  txta_id bigserial NOT NULL PRIMARY KEY,        -- identifiant du texte amendable
  txta_txt text NOT NULL                         -- identifiant du texte
    REFERENCES textes(txt_id),
  txta_commission text NOT NULL,                 -- nom de la commission saisie (POXXXX, SEANCE, exceptionnellement texte libre permis)
  txta_ordre integer,                            -- ordre relatif du texte amendable dans sa commission
  txta_statut statut_commission,                 -- statut de la commission pour ce texte amendable ('FOND', 'AVIS') (NULL pour la séance)
  txta_date_depot timestamptz,                   -- date de dépôt du texte amendable (date de saisie de la commission ou inscrit à l’ordre du jour de la séance)
  txta_date_depot_limite_interne timestamptz,    -- date de dépôt des amendements pour ce texte amendable
  txta_date_depot_limite_assemblee timestamptz,  -- date de dépôt des amendements pour ce texte amendable
  txta_nb_amdt_deposes integer,                  -- nombre d’amendements déposés sur ce texte amendable
  txta_cloture timestamptz,                      -- date de la fin de l’étape législative correspondant à ce texte amendable (NULL = non-clotûrée)
  txta_amendable boolean,                        -- le texte est effectivement ouvert aux amendements (et si la date actuelle est antérieure à la date de dépôt)
  UNIQUE (txta_txt, txta_commission)
);

-- Type "place de l’amendement" : "à", "avant", ou "après" la division ou l’alinéa
CREATE TYPE place_amdt AS ENUM ('A', 'AVANT', 'APRES', 'SUPPRESSION', 'REDACTION_GLOBALE');

-- Type "état de l’amendement" : "brouillon", "déposé", "discuté", "retiré"
CREATE TYPE etat_amdt AS ENUM ('BROUILLON', 'DEPOSE', 'DISCUTE', 'RETIRE');

-- Amendements
CREATE TABLE IF NOT EXISTS amendements(
  amdt_id bigserial NOT NULL,            -- identifiant unique interne à la table
  amdt_ordre integer,                     -- ordre organisationnel relatif (à place identique)
  amdt_eloi_tmp_id text,                  -- identifiant temporaire ELOI (tant que l’amendement est au stade "en conception" dans ELOI)
  amdt_eloi_id text,                      -- identifiant définitif ELOI (numéro séance)
  amdt_texte_id bigint NOT NULL           -- identifiant du texte amendé
    REFERENCES textes_amendables(txta_id),
  amdt_ssamdt_int_id bigint NULL,         -- identifiant du sur-amendement référençant un autre amendement de la table
  amdt_auteur text NOT NULL               -- identifiant de l’auteur (député) dans la table auteurs
    REFERENCES auteurs(aut_id),
  amdt_createur text                      -- identifiant du créateur dans la table users
    REFERENCES users(user_name_id),
  amdt_division text NOT NULL,            -- nom de la division (article souvent) de référence
  amdt_division_place place_amdt          -- place par rapport à la division de référence
    NOT NULL,
  amdt_alinea text NULL,                  -- nom de l’alinéa de référence
  amdt_alinea_place place_amdt NULL,      -- place par rapport à l’alinéa de référence
  amdt_titre text,                        -- titre de l’amendement
  amdt_dispositif text,                   -- dispositif de l’amendement
  amdt_expose text,                       -- exposé des motifs de l’amendement
  amdt_etat etat_amdt NOT NULL,           -- état d’avancement de la soumission de l’amendement (brouillon, déposé, discuté, retiré)
  amdt_sort text NULL,                    -- sort de l’amendement (cosignature du groupe, soutenu par le groupe, sagesse du groupe, tombé par un autre amendement du groupe, demande de retrait du groupe, irrecevable art. 40, autre irrecevable, etc. -- liste exacte à déterminer)
  amdt_date_creation timestamptz          -- date de la création de l’amendement
    NOT NULL,
  amdt_date_modification timestamptz      -- date de la dernière modification de l’amendement
    NOT NULL,
  amdt_date_depot timestamptz NULL,       -- date du dépôt (lorsque l’amendement sort de son stade de conception et est officiellement déposé)
  amdt_date_sort timestamptz NULL,        -- date du sort
  PRIMARY KEY (amdt_texte_id, amdt_id)
);

-- Cosignatures
CREATE TABLE IF NOT EXISTS cosignatures(
  cosig_id bigserial NOT NULL PRIMARY KEY, -- identifiant unique interne à la table
  cosig_amdt_id bigint NOT NULL            -- identifiant de l’amendement dans la table amendements
    REFERENCES amendements(amdt_id),
  cosig_auteur text NOT NULL               -- identifiant de l’auteur (député) de la cosignature dans la table auteurs
    REFERENCES auteurs(aut_id),
  cosig_createur text                      -- identifiant du créateur de la cosignature dans la table users
    REFERENCES users(user_name_id),
  cosig_date_creation timestamptz,         -- date de la création de la cosignature
  cosig_date_modification timestamptz,     -- date de la dernière modification de la cosignature
  cosig_motivation text,                   -- texte justificatif de la cosignature
  cosig_texte_id bigint NOT NULL,          -- identifiant du texte amendé
  FOREIGN KEY (cosig_texte_id, cosig_amdt_id) REFERENCES amendements(amdt_texte_id, amdt_id),
  UNIQUE (cosig_texte_id, cosig_amdt_id, cosig_auteur)
);

-- Sessions
-- Cf node_modules/connect-pg-simple/table.sql
CREATE TABLE IF NOT EXISTS sessions (
  sid text NOT NULL COLLATE "default" PRIMARY KEY NOT DEFERRABLE INITIALLY IMMEDIATE,
  sess json NOT NULL,
  expire timestamp(6) NOT NULL
) WITH (OIDS=FALSE);

-- vim: set ts=2 sw=2 sts=2 et:
