export const frenchDateFormat = new Intl.DateTimeFormat("fr-FR", {
  day: "numeric",
  month: "long",
  weekday: "long",
  year: "numeric",
})
export const frenchTimeFormat = new Intl.DateTimeFormat("fr-FR", {
  hour: "numeric",
  minute: "2-digit",
})

export function dateToLocalIso8601String(date) {
  return new Date(date.getTime() - date.getTimezoneOffset() * 60000)
    .toISOString()
    .split("T")[0]
}
