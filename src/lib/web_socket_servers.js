import WebSocket from "ws"

export const indexWebSocketServer = new WebSocket.Server({ noServer: true })

export function sendDataToWebSocketClients(webSocketServer, data) {
  const dataString = JSON.stringify(data)
  webSocketServer.clients.forEach(function each(client) {
    if (client.readyState === WebSocket.OPEN) {
      client.send(dataString)
    }
  })
}
