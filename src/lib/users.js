export function toUserPublicJson(user) {
  if (!user) {
    return null
  }
  let userJson = { ...user }
  delete userJson.nextcloudPassword
  return userJson
}
