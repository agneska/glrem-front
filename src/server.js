import bodyParser from "body-parser"
import compression from "compression"
// import csrf from "csurf"
import fs from "fs"
import http from "http"
import Polka from "polka"
import sirv from "sirv"
import proxy from "http-proxy-middleware"
import { Store } from "svelte/store.js"
import PgSession from "connect-pg-simple"
import session from "express-session"
import passport from "passport"
import { Strategy as LocalStrategy } from "passport-local"
import { Strategy as SamlStrategy } from "passport-saml"
import url from "url"

import * as sapper from "../__sapper__/server.js"
import config from "./config"
import { checkDatabase, db } from "./database"
import { upsertUser as upsertUserToNextcloud } from "./lib/nextcloud"
import { setupTelegram } from "./lib/telegram"
import { toUserPublicJson } from "./lib/users"
import { indexWebSocketServer } from "./lib/web_socket_servers"
import serverConfig from "./server_config"
import { assertValid } from "./validators/assert"
import { validateSamlProfile } from "./validators/saml_profile"

import "@fortawesome/fontawesome-free/css/svg-with-js.css" // used for `fa-spin` class
import "./styles/index.css"

const { PORT, NODE_ENV } = process.env
const dev = NODE_ENV === "development"
const PgSessionStore = PgSession(session)
const { saml, devUser } = serverConfig

async function upsertUser(user) {
  const record = await db.one(
    `
      INSERT INTO users(
        user_name_id,
        user_full_name,
        user_member_of,
        user_id_tribun,
        user_division,
        user_manager,
        user_saml_id,
        user_email,
        user_role
      )
      VALUES (
        $<nameId>,
        $<fullName>,
        $<memberOf>,
        $<idTribun>,
        $<division>,
        $<manager>,
        $<samlId>,
        $<email>,
        $<role>
      )
      ON CONFLICT (user_name_id)
      DO
        UPDATE
        SET
          user_full_name = $<fullName>,
          user_member_of = $<memberOf>,
          user_id_tribun = $<idTribun>,
          user_division = $<division>,
          user_manager = $<manager>,
          user_saml_id = $<samlId>,
          user_email = $<email>,
          user_role = $<role>
      RETURNING
        user_nextcloud_password
    `,
    user,
  )
  user.nextcloudPassword = record.user_nextcloud_password

  // Update Nextcloud account of user asynchronously (and don't wait for result).
  upsertUserToNextcloud(user).catch(error => {
    console.log(error)
  })
}

passport.deserializeUser(async function(userNameId, done) {
  const user = await db.oneOrNone(
    `
      SELECT *
      FROM users
      WHERE user_name_id = $<nameId>
    `,
    {
      nameId: userNameId,
    },
  )
  if (user === null) {
    return done(`Unknown user with nameID "${userNameId}"`, null)
  }
  done(null, {
    email: user.user_email,
    division: user.user_division,
    fullName: user.user_full_name,
    idTribun: user.user_id_tribun,
    manager: user.user_manager,
    memberOf: user.user_member_of,
    nameId: user.user_name_id,
    nextcloudPassword: user.user_nextcloud_password,
    role: user.user_role,
    samlId: user.user_saml_id,
  })
})

passport.serializeUser(function(user, done) {
  done(null, user.nameId)
})

if (saml !== null) {
  // Cf http://www.passportjs.org/packages/passport-saml/
  passport.use(
    new SamlStrategy(
      {
        callbackUrl: url.resolve(saml.url, "/auth/login-callback"),
        cert:
          saml.decryptionCertificatePath && saml.decryptionPrivateKeyPath
            ? saml.cert // Identity provider X.509 certificate
            : undefined,
        decryptionPvk:
          saml.decryptionCertificatePath && saml.decryptionPrivateKeyPath
            ? fs.readFileSync(saml.decryptionPrivateKeyPath, "utf-8")
            : undefined,
        entryPoint: saml.entryPoint,
        identifierFormat: saml.identifierFormat,
        issuer: saml.issuer,
        logoutCallbackUrl: url.resolve(saml.url, "/auth/logout-callback"),
        logoutUrl: saml.logoutUrl,
        privateCert:
          saml.signatureCertificatePath && saml.signaturePrivateKeyPath
            ? fs.readFileSync(saml.signaturePrivateKeyPath, "utf-8")
            : undefined,
        // signatureAlgorithm: "sha1",
      },
      async function(profile, done) {
        console.log(profile)
        const validProfile = assertValid(validateSamlProfile(profile))
        const user = {
          division: validProfile.division,
          email: validProfile.email,
          fullName: validProfile.fullname,
          idTribun: validProfile.idTribun,
          manager: validProfile.manager,
          memberOf: validProfile.memberof,
          nameId: validProfile.nameID,
          nextcloudPassword: null, // Retrieved later from database.
          role: null,
          samlId: validProfile.saml_id,
        }
        const sudoer = await db.oneOrNone(
          `
            SELECT *
            FROM sudoers
            WHERE sudo_name_id = $<nameId>
          `,
          {
            nameId: user.nameId,
          },
        )
        if (sudoer !== null) {
          user.idTribun = sudoer.sudo_id_tribun
          user.role = sudoer.sudo_role
        } else if (config.supportUsersNameIds.includes(user.nameId)) {
          user.role = "SUPPORT"
        } else if (user.idTribun !== null) {
          user.role = "DEPUTE"
        } else if (
          user.memberOf !== null &&
          user.memberOf.includes(
            "CN=WIFI_AN_LREM,OU=NAC,OU=Applications,DC=prod,DC=assemblee-nationale,DC=fr",
          )
        ) {
          user.role =
            user.division === "Stagiaire Groupe"
              ? "STAGIAIRE_GROUPE"
              : "COLLABORATEUR_GROUPE"
        } else if (user.manager !== null) {
          const match = /^CN=(.*),OU=Deputes,OU=Personnes,DC=prod,DC=assemblee-nationale,DC=fr$/.exec(
            user.manager,
          )
          if (match !== null) {
            const depute = await db.oneOrNone(
              `
                SELECT *
                FROM auteurs
                WHERE
                  concat_ws(' ', aut_prenom, aut_nom) = $<deputeFullName>
                  AND aut_actif
              `,
              {
                deputeFullName: match[1],
              },
            )
            if (depute !== null) {
              user.idTribun = depute.aut_id.substring(2)
              user.role = "COLLABORATEUR_DEPUTE"
            }
          }
        }
        await upsertUser(user)
        done(
          null, // error
          user,
        )
      },
    ),
  )
} else if (dev && devUser) {
  passport.use(
    new LocalStrategy(
      {
        usernameField: "username",
        passwordField: "password",
      },
      async function(username, password, done) {
        // Note: Ignore username & password.
        const user = {
          ...devUser,
          nextcloudPassword: null, // Retrieved later from database.
        }
        console.log(`Àuthenticated dev user: ${JSON.stringify(user)}`)
        await upsertUser(user)
        done(
          null, // error
          user,
        )
      },
    ),
  )
}

// const csrfProtection = csrf({
//   cookie: false,
// })

checkDatabase()
  .then(() => setupTelegram())
  .then(() => {
    const polka = Polka({
      server: http.createServer(),
    }).use(
      session({
        secret: serverConfig.sessionSecret,
        resave: false,
        saveUninitialized: false,
        store: new PgSessionStore({
          pgPromise: db,
          tableName: "sessions",
        }),
        cookie: {
          maxAge: 86400000,
        },
      }),
      bodyParser.json({
        limit: "1mb",
      }),
      bodyParser.urlencoded({
        extended: false,
        type: "application/x-www-form-urlencoded",
      }),
      passport.initialize(),
      passport.session(),
      compression({ threshold: 0 }),
      sirv("static", { dev }),
      sirv(serverConfig.dataDir, { dev }),
      proxy("/www.assemblee-nationale.fr/", {
        target: "http://www.assemblee-nationale.fr",
        changeOrigin: true,
        pathRewrite: { "^/www.assemblee-nationale.fr/": "/" },
      }),
      // (req, res, next) => {
      //   if (["/auth/login-callback", "/auth/logout"].includes(req.path)) {
      //     next()
      //   } else {
      //     csrfProtection(req, res, next)
      //   }
      // },
      sapper.middleware({
        store: req => {
          return new Store({
            user: toUserPublicJson(req.user),
            auteur:
              req.user &&
              req.user.idTribun &&
              (req.user.role === "DEPUTE" ||
                req.user.role === "COLLABORATEUR_DEPUTE")
                ? "PA" + req.user.idTribun
                : null,
            conseiller: req.user && req.user.role === "COLLABORATEUR_GROUPE",
            // csrfToken: req.csrfToken(),
            csrfToken: "TODO",
          })
        },
      }),
    )

    polka.server.on("upgrade", (request, socket, head) => {
      const pathname = url.parse(request.url).pathname
      // console.log("on upgrade", pathname)

      if (pathname === "/") {
        indexWebSocketServer.handleUpgrade(request, socket, head, function done(
          ws,
        ) {
          indexWebSocketServer.emit("connection", ws, request)
        })
      } else {
        socket.destroy()
      }
    })

    polka.listen(PORT, error => {
      if (error) {
        console.log(`Error when calling listen on port ${PORT}:`, error)
      }
    })
  })
  .catch(error => {
    console.log(error)
    process.exit(1)
  })

// vim: set ts=2 sw=2 sts=2 et:
