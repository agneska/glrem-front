import { db } from "../../../../database"
import { jsonError } from "../../../../lib/errors"
import { commAbrev } from "../../../../lib/data"
import { sanitiseMinimalHtml, check_integrity_amendment } from "../../../../lib/check"

const profiles = new Set(["DEPUTE", "COLLABORATEUR_DEPUTE", "COLLABORATEUR_GROUPE"])

export async function post(req, res) {
  const { user } = req
  if (!user) {
    return jsonError(res, 401, "Unauthenticated user")
  }
  if (!profiles.has(user.role)) {
    return jsonError(res, 403, "Unauthorised user")
  }

  const auteurId = user.idTribun && (user.role === "DEPUTE" || user.role === "COLLABORATEUR_DEPUTE")
                     ? "PA" + user.idTribun
                     : null

  const {
    action,
    texte,
    commission,
    amendementId,
    division,
    division_place,
    alinea,
    titre,
  } = req.body

  const alinea_place = alinea ? (req.body.alinea_place || "A") : null,
    dispositif = sanitiseMinimalHtml(req.body.dispositif),
    expose = sanitiseMinimalHtml(req.body.expose)

  let result = check_integrity_amendment(action, "deputé", {
    division,
    division_place,
    alinea,
    alinea_place,
    dispositif,
    expose,
  })
  if (result.status !== "success") {
    return jsonError(res, 400, "Non-coherent data")
  }

  if (["BROUILLON", "DEPOSE", "DISCUTE", "RETIRE"].indexOf(action) === -1) {
    return jsonError(res, 400, "Non-coherent data")
  }

  const texte_amendable = await db.oneOrNone(
    `SELECT textes_amendables.*, txt_responsables
     FROM textes_amendables
     LEFT JOIN textes ON (txta_txt = txt_id)
     WHERE txta_txt = $<texte> AND txta_commission = $<commission>
    `, {
      texte,
      commission: commAbrev[commission],
    },
  )
  if (!texte_amendable) {
    return jsonError(res, 404, "Missing text")
  }

  const responsables = texte_amendable.txt_responsables || [],
    estResponsable = user.role === "COLLABORATEUR_GROUPE" || responsables.indexOf(auteurId) !== -1

  if (!estResponsable && !texte_amendable.txta_amendable && action !== "RETIRE") {
    return jsonError(res, 409, "La période de dépôt d’amendements est terminée.")
  }

  const estNouveau = !amendementId,
    amendement = estNouveau ? null : await db.oneOrNone(
      ` SELECT *
        FROM amendements
        WHERE amdt_texte_id = $<amdt_texte_id> AND amdt_id = $<amdt_id>
      `,
      {
        amdt_texte_id: texte_amendable.txta_id,
        amdt_id: amendementId,
      },
    ),
    estAuteur = amendement && amendement.amdt_auteur === auteurId

  if (!estNouveau && !estAuteur && !estResponsable) {
    return jsonError(res, 409, "Cannot modify others’ amendments")
  }
  if (!auteurId && !estResponsable) {
    return jsonError(res, 409, "Amendment must be saved with an author")
  }
  if (!auteurId && estNouveau) {
    return jsonError(res, 409, "Advisors cannot submit amendments")
  }

  const dataDbAmdt = {
    texte: texte_amendable.txta_id,
    amendementId,
    division,
    division_place,
    alinea,
    alinea_place,
    dispositif,
    expose,
    titre,
    action,
    auteurId: auteurId,
  }

  if (estNouveau) {
    // New amendment
    const resultDbAmdt = await db.one(
      `INSERT INTO amendements
       SELECT COALESCE(MAX(amdt_id)+1, 1), NULL, NULL, NULL, $<texte>, NULL, $<auteurId>, NULL,
              $<division>, $<division_place>, $<alinea>, $<alinea_place>,
              $<titre>, $<dispositif>, $<expose>, $<action>, NULL,
              'NOW()', 'NOW()', 'NOW()', NULL
       FROM amendements
       WHERE amdt_texte_id = $<texte>
       RETURNING amdt_id;
      `,
      dataDbAmdt,
    )
    if (!resultDbAmdt) {
      return jsonError(res, 500, "Cannot write to database")
    }
    result.id = resultDbAmdt.amdt_id
    await db.none(
      `UPDATE textes_amendables
         SET txta_nb_amdt_deposes = (SELECT COUNT(amdt_id) FROM amendements
                                     WHERE amdt_etat IN ('DEPOSE', 'DISCUTE') AND amdt_texte_id = $<texte>)
         WHERE txta_id = $<texte>
      `,
      dataDbAmdt,
    )
  } else if (estAuteur && amendement.amdt_etat === "RETIE") {
    // Update amendment
    const resultDbAmdt = await db.one(
      `UPDATE amendements
         SET amdt_etat = $<action>,
             amdt_date_modification = 'NOW()'
         WHERE amdt_texte_id = $<texte> AND amdt_id = $<amendementId> AND amdt_auteur = $<auteurId>
         RETURNING amdt_id;
      `,
      dataDbAmdt,
    )
    if (!resultDbAmdt) {
      return jsonError(res, 500, "Cannot write to database")
    }
    result.id = resultDbAmdt.amdt_id
    await db.none(
      `UPDATE textes_amendables
         SET txta_nb_amdt_deposes = (SELECT COUNT(amdt_id) FROM amendements
                                     WHERE amdt_etat IN ('DEPOSE', 'DISCUTE') AND amdt_texte_id = $<texte>)
         WHERE txta_id = $<texte>
      `,
      dataDbAmdt,
    )
  } else if (estAuteur && amendement.amdt_etat !== "DISCUTE") {
    // Update amendment
    const resultDbAmdt = await db.one(
      `UPDATE amendements
         SET amdt_division = $<division>,
             amdt_division_place = $<division_place>,
             amdt_alinea = $<alinea>,
             amdt_alinea_place = $<alinea_place>,
             amdt_dispositif = $<dispositif>,
             amdt_titre = $<titre>,
             amdt_expose = $<expose>,
             amdt_etat = $<action>,
             amdt_date_modification = 'NOW()'
         WHERE amdt_texte_id = $<texte> AND amdt_id = $<amendementId> AND amdt_auteur = $<auteurId>
         RETURNING amdt_id;
      `,
      dataDbAmdt,
    )
    if (!resultDbAmdt) {
      return jsonError(res, 500, "Cannot write to database")
    }
    result.id = resultDbAmdt.amdt_id
    await db.none(
      `UPDATE textes_amendables
         SET txta_nb_amdt_deposes = (SELECT COUNT(amdt_id) FROM amendements
                                     WHERE amdt_etat IN ('DEPOSE', 'DISCUTE') AND amdt_texte_id = $<texte>)
         WHERE txta_id = $<texte>
      `,
      dataDbAmdt,
    )
  } else if (amendement && estResponsable && amendement.amdt_etat === "DEPOSE") {
    // Change metadata in amendment
    const resultDbAmdt = await db.one(
      `UPDATE amendements
       SET amdt_division = $<division>,
           amdt_division_place = $<division_place>,
           amdt_alinea = $<alinea>,
           amdt_alinea_place = $<alinea_place>,
           amdt_date_modification = 'NOW()'
       WHERE amdt_texte_id = $<texte> AND amdt_id = $<amendementId>
       RETURNING amdt_id;
      `,
      dataDbAmdt,
    )
    if (!resultDbAmdt) {
      return jsonError(res, 500, "Cannot write to database")
    }
    result.id = resultDbAmdt.amdt_id
  }

  res.writeHead(200, {
    "Content-Type": "application/json; charset=utf-8",
  })
  return res.end(JSON.stringify(result))
}

// vim: set ts=2 sw=2 sts=2 et:
