import { db } from "../../../../database"
import { commAbrev } from "../../../../lib/data"
import { sanitiseMinimalHtml } from "../../../../lib/check"
import { jsonError } from "../../../../lib/errors"

export async function get(req, res) {
  const { user } = req
  if (!user) {
    return jsonError(res, 401, "Unauthenticated user")
  }

  const auteurId = user.idTribun && (user.role === "DEPUTE" || user.role === "COLLABORATEUR_DEPUTE")
                     ? "PA" + user.idTribun
                     : null

  if (!auteurId && user.role !== "COLLABORATEUR_GROUPE") {
    return jsonError(res, 403, "Unauthorised user")
  }

  const { texte, commission, amendement } = req.params
  if (!/^\d+$/.exec(amendement)) {
    return jsonError(res, 400, "Bad parameters")
  }

  let amendements = await db.oneOrNone(
    ` SELECT amendements.*, auteurs.*, txt_responsables, txta_amendable
      FROM amendements
      LEFT JOIN auteurs ON (amdt_auteur = aut_id)
      LEFT JOIN textes_amendables ON (amdt_texte_id = txta_id)
      LEFT JOIN textes ON (txta_txt = txt_id)
      WHERE txta_txt = $<txta_txt> AND txta_commission = $<txta_commission> AND amdt_id = $<amdt_id>
    `,
    {
      txta_txt: texte,
      txta_commission: commAbrev[commission],
      amdt_id: amendement,
    }
  )

  if (!amendements) {
    return jsonError(res, 404, "Missing amendment")
  }

  if (auteurId !== amendements.amdt_auteur &&
      amendements.amdt_etat !== "DEPOSE" &&
      amendements.amdt_etat !== "DISCUTE") {
    return jsonError(res, 403, "Draft or removed amendment")
  }

  const estResponsable = (req.user && req.user.role === "COLLABORATEUR_GROUPE")
                         || (amendements.txt_responsables || []).indexOf(auteurId) !== -1

  if (auteurId !== amendements.amdt_auteur && !estResponsable) {
    return jsonError(res, 403, "You are not the author")
  } else {
    amendements.amdt_dispositif = sanitiseMinimalHtml(amendements.amdt_dispositif || "")
    amendements.amdt_expose = sanitiseMinimalHtml(amendements.amdt_expose || "")
  }

  res.writeHead(200, {
    "Content-Type": "application/json; charset=utf-8",
  })
  res.end(JSON.stringify(amendements))
}

// vim: set ts=2 sw=2 sts=2 et:
