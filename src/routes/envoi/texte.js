import { db } from "../../database"

export async function post(req, res) {
  const { user } = req
  if (!user) {
    res.writeHead(401, {
      "Content-Type": "application/json; charset=utf-8",
    })
    return res.end(
      JSON.stringify(
        {
          error: {
            code: 401,
            message: "Unauthenticated user",
          },
        },
        null,
        2,
      ),
    )
  }

  if (user.role !== "COLLABORATEUR_GROUPE") {
    return jsonError(res, 403, "Unauthorised user")
  }

  const {
    nouveau_numero,
    nouveau_titre,
    nouveau_seance,
    nouveau_depot_seance,
    nouveau_ouverture_seance,
    nouveau_comm_fond,
    nouveau_depot_comm_fond,
    nouveau_ouverture_comm_fond,
    nouveau_comm_avis,
    nouveau_depot_comm_avis,
    nouveau_ouverture_comm_avis,
    nouveau_respos_texte,
    nouveau_note_leg,
    nouveau_date_publi,
  } = req.body

  let record = {
    numero: nouveau_numero,
    titre: nouveau_titre,
    resp_texte: nouveau_respos_texte,
    date_publi: nouveau_date_publi,
    date_depot_limite: null,
    txt_url_note: nouveau_note_leg,
    txta_amendable: true,
  }

  if (nouveau_numero) {
    await db.none(
      ` UPDATE textes
          SET txt_responsables = $<resp_texte>,
              txt_url_note = $<txt_url_note>
          WHERE txt_id = $<numero>
      `,
      record,
    )
    if (nouveau_seance) {
      record.date_depot_limite = nouveau_depot_seance
      record.txta_amendable = nouveau_ouverture_seance
      await db.none(
        ` UPDATE textes_amendables
             SET txta_date_depot_limite_interne = $<date_depot_limite>,
                 txta_amendable = $<txta_amendable>
           WHERE txta_txt = $<numero> AND txta_commission = 'SEANCE'
        `,
        record,
      )
    }
    if (nouveau_comm_fond) {
      record.comm = nouveau_comm_fond
      record.date_depot_limite = nouveau_depot_comm_fond
      record.txta_amendable = nouveau_ouverture_comm_fond
      await db.none(
        ` UPDATE textes_amendables
             SET txta_date_depot_limite_interne = $<date_depot_limite>,
                 txta_amendable = $<txta_amendable>
           WHERE txta_txt = $<numero> AND txta_commission = $<comm>
        `,
        record,
      )
    }
    for (let comm of nouveau_comm_avis) {
      record.comm = comm
      record.date_depot_limite = nouveau_depot_comm_avis[comm]
      record.txta_amendable = nouveau_ouverture_comm_avis.indexOf(comm) !== -1
      await db.none(
        ` UPDATE textes_amendables
             SET txta_date_depot_limite_interne = $<date_depot_limite>,
                 txta_amendable = $<txta_amendable>
           WHERE txta_txt = $<numero> AND txta_commission = $<comm>
        `,
        record,
      )
    }
  } else {
    const special_txt_ids = await db.manyOrNone(
      ` SELECT txt_id
          FROM textes
          WHERE txt_id LIKE 'TEMP%'
      `,
    )
    const dernier_txt_id = special_txt_ids
      .map(s => { return s.txt_id.substring(4) })
      .reduce(
        (a, c) => {
          return parseInt(a) < parseInt(c) ? parseInt(c) : parseInt(a)
        }, 0)
    record.txt_id = "TEMP" + (dernier_txt_id + 1)
    const nouveau_texte = await db.one(
      `INSERT INTO textes
       VALUES ($<txt_id>, NULL, $<titre>, $<titre>, NULL, NULL, NULL, 'NOW()', false, NULL, NULL, $<txt_url_note>)
       RETURNING txt_id;`,
      record,
    )

    if (nouveau_seance) {
      record.date_depot_limite = nouveau_depot_seance
      record.txta_amendable = nouveau_ouverture_seance
      const nouveau_texte_fond = await db.one(
        `INSERT INTO textes_amendables
         VALUES (DEFAULT, $<txt_id>, 'SEANCE', NULL, NULL, 'NOW()', $<date_depot_limite>, NULL, 0, NULL, $<txta_amendable>)
         RETURNING txta_id;`,
        record,
      )
    }

    if (nouveau_comm_fond) {
      record.comm = nouveau_comm_fond
      record.date_depot_limite = nouveau_depot_comm_fond
      record.txta_amendable = nouveau_ouverture_comm_fond
      const nouveau_texte_fond = await db.one(
        `INSERT INTO textes_amendables
         VALUES (DEFAULT, $<txt_id>, $<comm>, NULL, 'FOND', 'NOW()', $<date_depot_limite>, NULL, 0, NULL, $<txta_amendable>)
         RETURNING txta_id;`,
        record,
      )
    }

    for (let comm of nouveau_comm_avis) {
      if (nouveau_comm_fond && comm == nouveau_comm_fond) {
        continue
      }
      record.comm = comm
      record.date_depot_limite = comm in nouveau_depot_comm_avis ? nouveau_depot_comm_avis[comm] : null
      record.txta_amendable = nouveau_ouverture_comm_avis.indexOf(comm) !== -1

      const nouveau_texte_avis = await db.one(
        `INSERT INTO textes_amendables
         VALUES (DEFAULT, $<txt_id>, $<comm>, NULL, 'AVIS', 'NOW()', $<date_depot_limite>, NULL, 0, NULL, $<txta_amendable>)
         RETURNING txta_id;`,
        record,
      )
    }
  }

  res.setHeader("Content-Type", "application/json; charset=utf-8")
  return res.end(JSON.stringify({}))
}

// vim: set ts=2 sw=2 sts=2 et:
