import { db } from "../../database"
import { jsonError } from "../../lib/errors"
import { commAbrev } from "../../lib/data"
import pre_alinea_order from "../../lib/sort"

const profiles = new Set(["COLLABORATEUR_GROUPE", "DEPUTE", "COLLABORATEUR_DEPUTE"])

export async function post(req, res) {

  // Basic access control check
  const { user } = req
  if (!user) {
    return jsonError(res, 401, "Utilisateur non-authentifié.")
  }
  if (!profiles.has(user.role)) {
    return jsonError(res, 403, "Utilisateur non-autorisé.")
  }

  const auteurId = req.user && req.user.idTribun &&
                   (req.user.role === "DEPUTE" || req.user.role === "COLLABORATEUR_DEPUTE")
                     ? "PA" + req.user.idTribun
                     : null

  // Basic parameters check
  const { texteId, commission, amendementsIds } = req.body

  if (
    !/^(?:TEMP)?[0-9]+$/.test(texteId) ||
    !(commission in commAbrev) ||
    !Array.isArray(amendementsIds) ||
    amendementsIds.length < 2 ||
    amendementsIds.some(x => !Number(x))
  ) {
    return jsonError(res, 400, "Mauvais paramètres.")
  }

  // Search the text
  const texte_amendable = await db.oneOrNone(
    ` SELECT txta_id, txt_responsables
      FROM textes_amendables
      LEFT JOIN textes ON (txta_txt = txt_id)
      WHERE txt_id = $<txt_id> AND txta_commission = $<txta_commission>
    `,
    {
      txt_id: texteId,
      txta_commission: commAbrev[commission],
    },
  )
  if (!texte_amendable) {
    return jsonError(res, 404, "Texte inconnu.")
  }

  const responsables = texte_amendable.txt_responsables || [],
    estResponsable = user.role === "COLLABORATEUR_GROUPE" || responsables.indexOf(auteurId) !== -1

  if (!estResponsable) {
    return jsonError(res, 403, "Utilisateur non-autorisé.")
  }

  // Search the amendements and check the division
  const amendements = await db.manyOrNone(
      ` SELECT *
        FROM amendements
        WHERE amdt_texte_id = $<amdt_texte_id> AND amdt_id IN ($<amdt_ids:list>)
      `,
      {
        amdt_texte_id: texte_amendable.txta_id,
        amdt_ids: amendementsIds,
      },
    ),
    places_identiques = amendements !== null && amendements.every(
      x =>
        x.amdt_division === amendements[0].amdt_division
        && x.amdt_division_place === amendements[0].amdt_division_place
    )

  if (amendementsIds.length !== amendements.length) {
    return jsonError(res, 409, "Incohérence dans la liste d’amendements à trier.")
  }

  if (!places_identiques) {
    return jsonError(res, 409, "Seuls les amendements sur la même division peuvent être déplacés entre eux.")
  }

  let amendementsObj = {}
  amendementsIds.forEach(
    (x, i) => {
      amendementsObj[x] = [amendements.find(y => y.amdt_id === x), i]
    }
  )

  // This order is such that explicit alineas are hard-enforced but
  // unspecified alineas can be anywhere
  const amendementsIdsOrd = amendementsIds.slice().sort(
    (x, y) => {
      const alinea_x = amendementsObj[x][0].amdt_alinea,
        alinea_y = amendementsObj[y][0].amdt_alinea,
        alinea_place_x = pre_alinea_order[amendementsObj[x][0].amdt_alinea_place],
        alinea_place_y = pre_alinea_order[amendementsObj[y][0].amdt_alinea_place]
      if (alinea_x !== null && alinea_y !== null) {
        if (alinea_x !== alinea_y) {
          return Number(alinea_x) < Number(alinea_y) ? -1 : 1
        } else if (alinea_place_x !== alinea_place_y) {
          return alinea_place_x < alinea_place_y ? -1 : 1
        }
      }
      return amendementsObj[x][1] < amendementsObj[y][1] ? -1 : 1
    }
  )

  if (amendementsIds.length !== amendementsIdsOrd.length
      || !amendementsIds.every((x, i) => x === amendementsIdsOrd[i])
  ) {
    return jsonError(res, 409, "L’ordre des alinéas doit rester, même si "
      + "possiblement entrecoupé d’alinéas non-spécifiés.")
  }

  // Save the new amendments’ places
  for (let i=0; i<amendementsIds.length; i++) {
    await db.none(
      ` UPDATE amendements
        SET amdt_ordre = $<amdt_ordre>
        WHERE amdt_id = $<amdt_id>
      `,
      {
        amdt_ordre: i+1,
        amdt_id: amendementsIds[i],
      }
    )
  }

  res.writeHead(200, {
    "Content-Type": "application/json; charset=utf-8",
  })
  return res.end(JSON.stringify({}))
}

// vim: set ts=2 sw=2 sts=2 et:
