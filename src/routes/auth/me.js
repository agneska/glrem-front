import { toUserPublicJson } from "../../lib/users"

export async function get(req, res) {
  res.writeHead(200, {
    "Content-Type": "application/json; charset=utf-8",
  })
  res.end(JSON.stringify(toUserPublicJson(req.user), null, 2))
}
