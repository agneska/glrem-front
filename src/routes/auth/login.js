import passport from "passport"

import serverConfig from "../../server_config"

const dev = process.env.NODE_ENV === "development"

const { devUser, saml } = serverConfig

export async function get(req, res, next) {
  function authenticationCallback(err, user, info) {
    console.log("auth/login", err, user, info)
    if (err) {
      return next(err)
    }
    if (!user) {
      // res.setHeader("Content-Type", "application/json; charset=utf-8")
      // return res.end(JSON.stringify({ error: info }, null, 2))
      res.writeHead(302, {
        Location: req.baseUrl + "/",
      })
      return res.end()
    }
    req.login(user, function(err) {
      if (err) {
        return next(err)
      }
      // res.setHeader("Content-Type", "application/json; charset=utf-8")
      // return res.end(JSON.stringify(user, null, 2))
      res.writeHead(302, {
        Location: req.baseUrl + "/",
      })
      return res.end()
    })
  }

  if (saml !== null) {
    passport.authenticate("saml", { failureRedirect: "/", failureFlash: true })(
      req,
      res,
      authenticationCallback,
    )
  } else if (dev && devUser !== null) {
    req.body = {
      password: "UNUSED",
      username: devUser.nameId,
    }
    passport.authenticate("local", authenticationCallback)(req, res, next)
  } else {
    res.writeHead(400, {
      "Content-Type": "text/plain; charset=utf-8",
    })
    return res.end("No authentication method configured!")
  }
}
