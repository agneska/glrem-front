import fs from "fs"
import passport from "passport"

import serverConfig from "../../server_config"

const { saml } = serverConfig

export function get(req, res) {
  res.writeHead(200, {
    "Content-Type": "text/xml; charset=utf-8",
  })
  res.end(
    passport._strategies.saml.generateServiceProviderMetadata(
      saml.decryptionCertificatePath && saml.decryptionPrivateKeyPath
        ? fs.readFileSync(saml.decryptionCertificatePath, "utf-8")
        : undefined,
      saml.signatureCertificatePath && saml.signaturePrivateKeyPath
        ? fs.readFileSync(saml.signatureCertificatePath, "utf-8")
        : undefined,
    ),
  )
}
