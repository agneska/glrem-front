import sirv from "sirv"

const dev = process.env.NODE_ENV === "development"

export async function get(req, res, next) {
  const { user } = req
  if (!user) {
    res.writeHead(401, {
      "Content-Type": "application/json; charset=utf-8",
    })
    return res.end(
      JSON.stringify(
        {
          error: {
            code: 401,
            message: "Unauthenticated user",
          },
        },
        null,
        2,
      ),
    )
  }

  const { path } = req.params
  req.path = encodeURI(`/${path}`)
  return sirv("__tglib__/files", {
    dev,
    etag: true,
    extensions: [],
    immutable: true,
    maxAge: 86400, // one day
  })(req, res, next)
}
