import { db } from "../database"

export async function get(req, res) {
  const { user } = req
  if (!user) {
    res.writeHead(401, {
      "Content-Type": "application/json; charset=utf-8",
    })
    return res.end(
      JSON.stringify(
        {
          error: {
            code: 401,
            message: "Unauthenticated user",
          },
        },
        null,
        2,
      ),
    )
  }

  res.writeHead(200, {
    "Content-Type": "application/json; charset=utf-8",
  })
  const textes = await db.manyOrNone(
    `
      SELECT *
      FROM textes_amendables
      INNER JOIN textes ON (txta_txt = txt_id)
      ORDER BY txt_date_depot DESC
    `,
  )
  let textesCommission = {}
  for (let texte of textes) {
    const commission =
      texte.txta_commission == "SEANCE"
        ? "Séance publique"
        : texte.txta_commission
    if (!textesCommission[commission]) {
      textesCommission[commission] = []
    }
    textesCommission[commission].push(texte)
  }
  res.end(JSON.stringify(textesCommission))
}

// vim: set ts=2 sw=2 sts=2 et:
