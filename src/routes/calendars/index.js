import { retrieveUserCalendars } from "../../lib/nextcloud"

export async function get(req, res) {
  const { user } = req
  if (!user) {
    res.writeHead(401, {
      "Content-Type": "application/json; charset=utf-8",
    })
    return res.end(
      JSON.stringify(
        {
          error: {
            code: 401,
            message: "Unauthenticated user",
          },
        },
        null,
        2,
      ),
    )
  }

  const calendars = await retrieveUserCalendars(user)

  res.writeHead(200, {
    "Content-Type": "application/json; charset=utf-8",
  })
  res.end(JSON.stringify(calendars, null, 2))
}
