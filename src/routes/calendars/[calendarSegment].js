import assert from "assert"
import dedent from "dedent-js"
import fastXmlParser from "fast-xml-parser"
import fetch from "node-fetch"
import ical from "ical.js"
import url from "url"

import serverConfig from "../../server_config"

// import {
//   validateStringToNumber,
//   validateChain,
//   validateInteger,
//   validateMaybeTrimmedString,
//   validateString,
//   validateTest,
// } from "../../../validators/core"

const { nextcloud } = serverConfig

export async function get(req, res) {
  const { user } = req
  if (!user) {
    res.writeHead(401, {
      "Content-Type": "application/json; charset=utf-8",
    })
    return res.end(
      JSON.stringify(
        {
          error: {
            code: 401,
            message: "Unauthenticated user",
          },
        },
        null,
        2,
      ),
    )
  }

  const { calendarSegment } = req.params

  const [query, error] = validateQuery(req.query)
  if (error !== null) {
    console.error(
      `Error in ${req.path} query:\n${JSON.stringify(
        query,
        null,
        2,
      )}\n\nError:\n${JSON.stringify(error, null, 2)}`,
    )
    res.writeHead(400, {
      "Content-Type": "application/json; charset=utf-8",
    })
    return res.end(
      JSON.stringify(
        {
          ...query,
          error: {
            code: 400,
            details: error,
            message: "Invalid query",
            path: req.path,
          },
        },
        null,
        2,
      ),
    )
  }

  const now = new Date()
  now.setMinutes(now.getMinutes() - now.getTimezoneOffset())
  const firstDayOfPreviousMonth = new Date(
    now.getFullYear(),
    now.getMonth() - 1,
    1,
  )
  firstDayOfPreviousMonth.setMinutes(
    -firstDayOfPreviousMonth.getTimezoneOffset(),
  )
  const lastDayOfNextMonth = new Date(now.getFullYear(), now.getMonth() + 2, 0)
  lastDayOfNextMonth.setMinutes(-lastDayOfNextMonth.getTimezoneOffset())
  const response = await fetch(
    url.resolve(
      nextcloud.url,
      `remote.php/dav/calendars/${user.samlId}/${encodeURIComponent(
        calendarSegment,
      )}/`,
    ),
    {
      body: dedent`
          <c:calendar-query xmlns:c="urn:ietf:params:xml:ns:caldav">
            <d:prop xmlns:d="DAV:">
              <d:getetag/>
              <c:calendar-data/>
            </d:prop>
            <c:filter>
              <c:comp-filter name="VCALENDAR">
                <c:comp-filter name="VEVENT">
                  <c:time-range
                    start="${firstDayOfPreviousMonth.toISOString().split("T")[0].replace(/-/g, "")}T000000Z"
                    end="${lastDayOfNextMonth.toISOString().split("T")[0].replace(/-/g, "")}T000000Z"
                  />
                </c:comp-filter>
              </c:comp-filter>
            </c:filter>
          </c:calendar-query>
        `,
      headers: {
        Authorization:
          "Basic " +
          Buffer.from(user.samlId + ":" + user.nextcloudPassword).toString(
            "base64",
          ),
        Depth: "1",
        // "OCS-APIRequest": "true",
        "User-Agent": nextcloud.userAgent,
      },
      method: "REPORT",
    },
  )
  const jsonEvents = []
  const text = await response.text()
  if (response.status !== 207) { // Multi-Status
    console.log(response.status, response.statusText)
    console.log(text)
  } else {
    assert(fastXmlParser.validate(text))
    const options = {
      ignoreNameSpace: true,
    }
    const result = fastXmlParser.parse(text, options)
    // console.log(JSON.stringify(result, null, 2))

    let responses = result.multistatus.response
    if (!responses) {
      responses = []
    } else if (!Array.isArray(responses)) {
      assert(responses)
      responses = [responses]
    }
    for (let response of responses) {
      assert.strictEqual(response.propstat.status, "HTTP/1.1 200 OK")
      // `response.href` is the ".ics" URL of the event.
      const calendarDataText = response.propstat.prop["calendar-data"]
      const vcalendar = ical.parse(calendarDataText)
      const component = new ical.Component(vcalendar)
      // console.log(JSON.stringify(component, null, 2))
      const vevent = component.getFirstSubcomponent("vevent")
      // console.log(JSON.stringify(vevent, null, 2))
      const event = new ical.Event(vevent)
      const jsonEvent = {
        // allDay: true,
        description: event.description,
        end: event.endDate.toJSDate(),
        id: event.uid,
        location: event.location,
        start: event.startDate.toJSDate(),
        summary: event.summary,
      }
      for (const [jcalName, jsonName] of [
        ["x-format-reunion", "formatReunion"],
        ["x-ouverture-presse", "ouverturePresse"],
      ]) {
        const propertyValue = vevent.getFirstPropertyValue(jcalName)
        if (propertyValue !== null) {
          jsonEvent[jsonName] = propertyValue
        }
      }
      // if (vevent.getFirstPropertyValue())
      jsonEvents.push(jsonEvent)
    }
  }

  res.writeHead(200, {
    "Content-Type": "application/json; charset=utf-8",
  })
  res.end(JSON.stringify(jsonEvents, null, 2))
}

function validateQuery(query) {
  if (query === null || query === undefined) {
    return [query, "Missing query"]
  }
  if (typeof query !== "object") {
    return [query, `Expected an object, got ${typeof query}`]
  }

  query = {
    ...query,
  }
  const remainingKeys = new Set(Object.keys(query))
  const errors = {}

  // {
  //   const key = "id"
  //   remainingKeys.delete(key)
  //   const [value, error] = validateChain([
  //     validateString,
  //     validateStringToNumber,
  //     validateInteger,
  //     validateTest(value => value >= 0, "Le nombre doit être positif ou nul."),
  //   ])(query[key])
  //   query[key] = value
  //   if (error !== null) {
  //     errors[key] = error
  //   }
  // }

  // {
  //   const key = "q"
  //   remainingKeys.delete(key)
  //   const [value, error] = validateMaybeTrimmedString(query[key])
  //   query[key] = value
  //   if (error !== null) {
  //     errors[key] = error
  //   }
  // }

  // {
  //   const key = "year"
  //   remainingKeys.delete(key)
  //   const [value, error] = validateChain([
  //     validateString,
  //     validateStringToNumber,
  //     validateInteger,
  //     validateTest(value => value >= 1700 && value < 2000, "Expected a year between 1700 and 1999"),
  //   ])(query[key])
  //   query[key] = value
  //   if (error !== null) {
  //     errors[key] = error
  //   }
  // }

  for (let key of remainingKeys) {
    errors[key] = "Unexpected entry"
  }
  return [query, Object.keys(errors).length === 0 ? null : errors]
}
