require("dotenv").config()

import { validateServerConfig } from "./validators/server_config"

const serverConfig = {
  dataDir: process.env.INTRANETLREM_DATA_DIR,
  db: {
    host: process.env.INTRANETLREM_DB_HOST || "localhost",
    port: process.env.INTRANETLREM_DB_PORT || 5432,
    database: process.env.INTRANETLREM_DB_NAME || "lrem",
    user: process.env.INTRANETLREM_DB_USER || "lrem",
    password: process.env.INTRANETLREM_DB_PASSWORD, // Set it!
  },
  devUser: process.env.INTRANETLREM_DEV_USER,
  gitlab: process.env.INTRANETLREM_GITLAB_URL
    ? {
        url: process.env.INTRANETLREM_GITLAB_URL,
        accessToken: process.env.INTRANETLREM_GITLAB_ACCESS_TOKEN,
        projectPath: process.env.INTRANETLREM_GITLAB_PROJECT,
      }
    : null,
  nextcloud: process.env.INTRANETLREM_NEXTCLOUD_URL
    ? {
        url: process.env.INTRANETLREM_NEXTCLOUD_URL,
        user: process.env.INTRANETLREM_NEXTCLOUD_USER,
        password: process.env.INTRANETLREM_NEXTCLOUD_PASSWORD,
        userAgent: "glrem-front/fetch",
      }
    : null,
  quiz: process.env.INTRANETLREM_QUIZ_URL
    ? {
        url: process.env.INTRANETLREM_QUIZ_URL,
        segment: process.env.INTRANETLREM_QUIZ_SEGMENT,
        token: process.env.INTRANETLREM_QUIZ_TOKEN,
      }
    : null,
  // cf http://www.passportjs.org/packages/passport-saml/
  saml: process.env.INTRANETLREM_SAML_ENTRY_POINT
    ? {
        cert: process.env.INTRANETLREM_SAML_CERT,
        decryptionCertificatePath:
          process.env.INTRANETLREM_SAML_DECRYPTION_CERTIFICATE_PATH,
        decryptionPrivateKeyPath:
          process.env.INTRANETLREM_SAML_DECRYPTION_PRIVATE_KEY_PATH,
        entryPoint: process.env.INTRANETLREM_SAML_ENTRY_POINT,
        identifierFormat: process.env.INTRANETLREM_SAML_IDENTIFIER_FORMAT,
        issuer: process.env.INTRANETLREM_SAML_ISSUER,
        logoutUrl: process.env.INTRANETLREM_SAML_LOGOUT_URL,
        signatureCertificatePath:
          process.env.INTRANETLREM_SAML_SIGNATURE_CERTIFICATE_PATH,
        signaturePrivateKeyPath:
          process.env.INTRANETLREM_SAML_SIGNATURE_PRIVATE_KEY_PATH,
        url: process.env.INTRANETLREM_SAML_URL,
      }
    : null,
  telegram: process.env.INTRANETLREM_TELEGRAM_API_ID
    ? {
        apiId: process.env.INTRANETLREM_TELEGRAM_API_ID,
        apiHash: process.env.INTRANETLREM_TELEGRAM_API_HASH,
        binaryPath: process.env.INTRANETLREM_TELEGRAM_BINARY_PATH,
        phoneNumber: process.env.INTRANETLREM_TELEGRAM_PHONE_NUMBER,
        authorizationCode: process.env.INTRANETLREM_TELEGRAM_AUTHORIZATION_CODE,
      }
    : null,
  sessionSecret: process.env.INTRANETLREM_SESSION_SECRET,
  zendesk: process.env.INTRANETLREM_ZENDESK_API_TOKEN
    ? {
        actualitesId: process.env.INTRANETLREM_ZENDESK_ACTUALITES_ID,
        apiToken: process.env.INTRANETLREM_ZENDESK_API_TOKEN,
        email: process.env.INTRANETLREM_ZENDESK_EMAIL,
        url: process.env.INTRANETLREM_ZENDESK_URL,
      }
    : null,
}

const [validServerConfig, error] = validateServerConfig(serverConfig)
if (error !== null) {
  console.error(
    `Error in server configuration:\n${JSON.stringify(
      validServerConfig,
      null,
      2,
    )}\nError:\n${JSON.stringify(error, null, 2)}`,
  )
  process.exit(-1)
}

export default validServerConfig
