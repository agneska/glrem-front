const defaultTheme = require("tailwindcss/defaultTheme")

module.exports = {
  theme: {
    extend: {
      borderRadius: {
        default: "3px",
      },
      colors: {
        transparent: "transparent",

        "bleu-darker": "#0d2b5e",
        "bleu-dark": "#154598",
        bleu: "#1d5fd1",
        "bleu-light": "#4b84e5",
        "bleu-lighter": "#a3c1f5",

        "blue-dark": "#044faa",
        blue: "#1d5fd1",
        "blue-light": "#2abaff",

        "cyan-darker": "#1b77a3",
        "cyan-dark": "#2399d1",
        cyan: "#2abaff",
        "cyan-light": "#50c6ff",
        "cyan-lighter": "#8ad9ff",

        "gray-light": "#e5e5e5",

        "green-lightest": "#61e9d5",

        "jaune-darkest": "#453411",
        "jaune-darker": "#684f1d",
        "jaune-dark": "#f2d024",
        jaune: "#ffeb00",
        "jaune-light": "#fff382",
        "jaune-lighter": "#fff9c2",
        "jaune-lightest": "#fcfbeb",

        "orange-darkest": "#462a16",
        "orange-darker": "#613b1f",
        "orange-dark": "#de751f",
        orange: "#ff6955",
        "orange-light": "#faad63",
        "orange-lighter": "#fcd9b6",
        "orange-lightest": "#fff5eb",

        "rose-lightest": "#ffdee9",
        "rose-lighter": "#fcbbd1",
        "rose-light": "#fd82ab",
        rose: "#ff4d89",
        "rose-dark": "#cf225c",
        "rose-darker": "#870833",

        "vert-lighter": "#befbf2",
        "vert-light": "#90f8e9",
        vert: "#61e9d5",
        "vert-dark": "#50c3b2",
        "vert-darker": "#3e988b",

        "violet-lighter": "#b0b9ff",
        "violet-light": "#8997ff",
        violet: "#6f80ff",
        "violet-dark": "#6575e8",
        "violet-darker": "#515eba",
      },
      fontFamily: {
        sans: ["Lato", ...defaultTheme.fontFamily.sans],
      },
      fontSize: {
        md: ".9375rem",
      },
      maxWidth: {
        five: "5rem",
      },
      minHeight: {
        "50": "50px",
        "70": "70px",
        "500": "500px",
      },
      maxHeight: {
        "25": "25px",
        "350": "350px",
        "750": "750px",
      },
      lineHeight: {
        superTight: "0.6em",
      },
    },
  },
  variants: {
    backgroundColor: ["responsive", "hover", "focus", "group-hover"],
    borderColor: ["responsive", "hover", "focus", "active"],
    inset: ["responsive"],
    maxHeight: ["responsive"],
    minHeight: ["responsive"],
    objectFit: ["responsive"],
    objectPosition: ["responsive"],
    opacity: ["responsive", "group-hover", "hover"],
    textColor: ["responsive", "hover", "focus", "group-hover", "active"],
  },
  plugins: [],
}
