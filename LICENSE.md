# GLREM-Front

## _A front page for French Deputies_

By:
* Sébastien Beyou <mailto:seb35@seb35.fr>
* Eliza Iacoblev <mailto:eliza.iacoblev@gmail.com>
* Emmanuel Raviart <mailto:emmanuel@raviart.com>

Copyright (C) 2019 Eliza Iacoblev, Sébastien Beyou & Emmanuel Raviart

https://framagit.org/glrem-platform/glrem-front.git

> GLREM-Front is free software; you can redistribute it and/or modify
> it under the terms of the GNU Affero General Public License as
> published by the Free Software Foundation, either version 3 of the
> License, or (at your option) any later version.
>
> GLREM-Front is distributed in the hope that it will be useful,
> but WITHOUT ANY WARRANTY; without even the implied warranty of
> MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
> GNU Affero General Public License for more details.
>
> You should have received a copy of the GNU Affero General Public License
> along with this program. If not, see <http://www.gnu.org/licenses/>.
